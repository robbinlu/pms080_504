#include "FOC.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "Table.h"
#include "DAC.h"
#include "Interrupt.h"

static uint8_t send_cnt = 50;

void Angle_PID(void)
{
	a_error2=a_error1;
  a_error1=(int16_t)(((Sin_Normalization*sintable16[Cosangle_feedback])-(Cos_Normalization*sintable16[Angle_feedback]))/10000);  
	AP_value=(int32_t)(AKp*(a_error1-a_error2));
	AI_value=(int32_t)(AKi*a_error1);
	APID_output=AP_value+AI_value;
	APID_output=APID_output>>15;
	if(APID_output>APID_Limit)
	{
		APID_output=APID_Limit;
	}
	if(APID_output<(0-APID_Limit))
	{
		APID_output=(0-APID_Limit);
	}
	Angle_feedback=(Angle_feedback+APID_output+3600)%3600;
	Angle_feedback_t = (Angle_feedback-Angle_offset+3600)%3600;
	DAC_Output(Angle_feedback_t);
	if(Angle_feedback>2700)                   //cos(a)=sin(a+90°)
	{
		Cosangle_feedback=Angle_feedback-2700;
	}
	else if(Angle_feedback<2700)
	{
		Cosangle_feedback=Angle_feedback+900;
	}
	else if(Angle_feedback==2700)
	{
		Cosangle_feedback=0;
	}
	Angle_rotor=(((Angle_feedback-Angle_offset+3600)%3600)<<2)%3600;
}


void Park_inverse(uint16_t theta_in)
{
	uint16_t costheta; 
	
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900; 
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	Ua0=Ud*(sintable16[costheta])-Uq*(sintable16[theta_in]);
	Ub0=Ud*(sintable16[theta_in])+Uq*(sintable16[costheta]);
	Ua=Ua0>>15;
	Ub=Ub0>>15;
	//这里有一个疑问 比如我Uq是5000 那么theta在0~6° Ua=0 6~12°Ua=1 会不会对控制不好 
	//
}



/*******************communication App*************************/
uint8_t CRC_check(uint8_t *Buf, uint8_t len)
{
	uint32_t sum=0;
	uint8_t i = 0;
	for(i = 0; i < len; i++)
	{
		sum += Buf[i];
	}
	return (uint8_t)(sum);
}


void Communication_App(void)
{
	if(--send_cnt == 0)
	{
		send_buf[0] = 0xa1;
		send_buf[1] = Motor_flag; 
		send_buf[2] = (uint8_t)Speed;       //ch1
		send_buf[3] = (uint8_t)(Speed>>8);
		send_buf[4] = (uint8_t)Id;
		send_buf[5] = (uint8_t)(Id>>8);
		send_buf[6] = (uint8_t)Iq;
		send_buf[7] = (uint8_t)(Iq>>8);
		send_buf[8] = (uint8_t)Uq;
		send_buf[9] = (uint8_t)(Uq>>8);
		send_buf[10] = (uint8_t)Angle_offset;
		send_buf[11] = (uint8_t)(Angle_offset>>8);
		send_buf[12] = (uint8_t)AKp;
		send_buf[13] = (uint8_t)(AKp>>8);		
	  send_buf[14] = (uint8_t)AKi;
		send_buf[15] = (uint8_t)(AKi>>8);	
		send_buf[16] = (uint8_t)R_value;
		send_buf[17] = (uint8_t)(R_value>>8);
		send_buf[18] = (uint8_t)Uq_Limit;
		send_buf[19] = (uint8_t)(Uq_Limit>>8);
		send_buf[20] = (uint8_t)Angle_feedback;					//ch2
		send_buf[21] = (uint8_t)(Angle_feedback>>8);
		send_buf[22] = (uint8_t)Sin_value;							//ch3
		send_buf[23] = (uint8_t)(Sin_value>>8);
		send_buf[24] = (uint8_t)Cos_value;							//ch4
		send_buf[25] = (uint8_t)(Cos_value>>8);
		send_buf[26] = 0;
		send_buf[27] = 0;
		send_buf[28] = 0;
		send_buf[29] = CRC_check(send_buf,29);
		send_buf[30] = 0x78;
		send_buf[31] = 0x56;
		Uart_Send_Flag = 1;
		send_cnt = 50;
	}
}

void Uart_Rec(void)
{
	int16_t data16 = 0;
	if(Rec_Sta&0x8000)
	{
		if(((Rec_Sta&0x3fff) == 6)&&(Uart_Buffer[0]==0xa1))//8-2
		{
			data16 = (int16_t)(Uart_Buffer[2])|(Uart_Buffer[3]<<8);
			switch(Uart_Buffer[1])
			{
				case 0: hand_shake_Flag = data16;
								break;//start/stop
				case 1: if(data16 > 1)
								{
									data16 = 0;
								}
								uart_state_flag = data16;
								break;//start/stop
				case 2: if(data16 > SPEED_MAX)
								{
									data16 = SPEED_MAX;
								}
								Speed_Cmd = data16;
                R_value = Speed_Cmd;
								break;
				case 3:	Uq = data16;
								break;
								
				case 4: //Angle_offset = data16;
								break;
								
   			case 5: AKp = data16;
						    break;
								
			  case 6: AKi = data16;
								break;
								
			  case 7: Uq_Limit = data16;
								break;		
								
				default:break;				
			}
		}	
		Rec_Sta = 0;
	}
}
