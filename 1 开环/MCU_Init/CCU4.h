#ifndef _CCU4_H_
#define _CCU4_H_

#include <XMC4400.h>

void CCU40_Init(void);
void CCU40_CC40_Start(void);
void CCU40_CC41_Start(void);
void CCU40_CC42_Start(void);

#endif
