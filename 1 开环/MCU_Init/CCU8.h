#ifndef _CCU8_H_
#define _CCU8_H_

#include <XMC4400.h>


void CCU80_Init(void);
void CCU80_Start(void);

#endif
