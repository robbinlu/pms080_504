#ifndef __GPIO_H__
#define __GPIO_H__

#include <XMC4400.h>

#define HALLA 	P1_3
#define HALLB 	P1_2
#define HALLC 	P1_1

#define PWM_UH	P0_5
#define PWM_UL 	P0_2
#define PWM_VH 	P0_4
#define PWM_VL 	P0_1
#define PWM_WH 	P0_3
#define PWM_WL 	P0_0

#define R_ADC   P14_9
#define I_U     P14_14
#define I_V			P14_6

extern void GPIO_Init(void);


#endif
