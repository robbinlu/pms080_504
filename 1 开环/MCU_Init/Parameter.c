#include "Parameter.h"
#include "SVPWM.h"

int16_t Angle,Angle_120,Angle_240;
/*****STATE*****/
Motor_State Motor_flag;


/*****SPEED*****/
uint16_t Speed;
uint32_t speed_time[8];
uint64_t speed_time_sum;
uint8_t speed_get_flag;
volatile uint8_t speed_count;

/*****CURRENT*****/
int16_t Iq;
int16_t Id;


/*****ADC*****/
uint16_t R_value;

//sin cos信号与锁相环
uint16_t Sin_value;
uint16_t Cos_value;
uint16_t sin_max=2428;
uint16_t sin_min=1360;
uint16_t cos_max=2512;
uint16_t cos_min=1408;

uint16_t sin_dc_component=1894;
uint16_t cos_dc_component=1960;

int16_t  Sin_Normalization=0;
int16_t  Cos_Normalization=0;

int16_t Angle_feedback=0;
int16_t Angle_feedback_t=0;
int16_t Cosangle_feedback=0; 
uint16_t Angle_rotor=0;
uint16_t Angle_offset;


/*****UART*****/
/*****UART*****/
uint8_t Uart_Buffer[REC_LENGTH];//接收buffer
uint16_t Rec_Sta = 0;
uint8_t Uart_Send_Flag = 0;
uint8_t send_buf[32];   //发送buffer

uint8_t hand_shake_Flag = 0;//握手
uint8_t uart_state_flag = 0;

/*****PID*****/
uint16_t Speed_Ref;
uint16_t Speed_Cmd;
uint16_t Ki = 6;//400
uint16_t AKp;
uint16_t AKi;

uint16_t Uq_Limit = 15000;

int32_t APID_output;
int16_t AIvalue_limit;
int16_t APID_Limit;
int16_t a_error1=0;
int16_t a_error2=0;
int32_t AP_value;
int32_t AI_value;

/*****clarke-park*****/
int16_t Ud;
int16_t Uq;
int32_t Ua0;
int32_t Ub0;
int16_t Ua;
int16_t Ub;



void Parameter_Init(void)
{
	//Uq由上位机给定，其余参数初始化，SVPWM周期
	
	SVPWM16.PWM_Period=3750;
	Speed_Ref=0;	

	Speed=0;
	uint8_t reset_speed;
	for(reset_speed=0;reset_speed<8;reset_speed++)
	{
		speed_time[reset_speed]=0;
	}
	speed_time_sum = 0;
	speed_count=0;
	speed_get_flag=0;	
	
	Angle_offset=0;
	
	Ud=0;
}

void PID_Init(void)
{
	//AKP AKI UQ上位机给
	AIvalue_limit=5000;
	APID_Limit=3600;
  APID_output=0;
  a_error1=0;
  a_error2=0;
  AP_value=0;
  AI_value=0;
}

