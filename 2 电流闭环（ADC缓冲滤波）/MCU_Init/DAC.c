#include "DAC.h"
#include <xmc_dac.h>


//Channel0-P14.8
//Channel1-P14.9

XMC_DAC_CH_CONFIG_t const ch_config0=
{
  .output_offset	= 0U,
  .data_type 		= XMC_DAC_CH_DATA_TYPE_UNSIGNED,
  .output_scale 	= XMC_DAC_CH_OUTPUT_SCALE_NONE,
  .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
};

XMC_DAC_CH_CONFIG_t const ch_config1=
{
  .output_offset	= 0U,
	.data_type 		= XMC_DAC_CH_DATA_TYPE_SIGNED,
  //.data_type 		= XMC_DAC_CH_DATA_TYPE_UNSIGNED,
  .output_scale 	= XMC_DAC_CH_OUTPUT_SCALE_NONE,
  .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
};



void DAC_Init(void)
{
  XMC_DAC_CH_Init(XMC_DAC0, 1U, &ch_config1);
	XMC_DAC_CH_StartSingleValueMode(XMC_DAC0, 1U);
}



void DAC_Output(int16_t dat)
{
	XMC_DAC_CH_Write(XMC_DAC0,1U,dat);
}

void DAC_Output0(int16_t dat)
{
	XMC_DAC_CH_Write(XMC_DAC0,0U,dat);
}
