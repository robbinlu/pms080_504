#include "GPIO.h"
#include <xmc_gpio.h>


void GPIO_Init(void)
{
	XMC_GPIO_CONFIG_t config;

	//P1_1 1_2 1_3
  config.mode = XMC_GPIO_MODE_INPUT_PULL_UP;
  XMC_GPIO_Init(HALLA, &config);
  XMC_GPIO_Init(HALLB, &config);
	XMC_GPIO_Init(HALLC, &config);

	//0  不用的引脚设为INPUT
	XMC_GPIO_Init(P0_6, &config);
	XMC_GPIO_Init(P0_7, &config);
	XMC_GPIO_Init(P0_8, &config);
	XMC_GPIO_Init(P0_9, &config);
	XMC_GPIO_Init(P0_10, &config);
	XMC_GPIO_Init(P0_11, &config);
	//1
	XMC_GPIO_Init(P1_0, &config);
	XMC_GPIO_Init(P1_4, &config);
	XMC_GPIO_Init(P1_5, &config);
	XMC_GPIO_Init(P1_8, &config);
	XMC_GPIO_Init(P1_9, &config);
	XMC_GPIO_Init(P1_15, &config);
	//2
  XMC_GPIO_Init(P2_0, &config);
	XMC_GPIO_Init(P2_1, &config);
	XMC_GPIO_Init(P2_4, &config);
	XMC_GPIO_Init(P2_6, &config);
	XMC_GPIO_Init(P2_7, &config);
	XMC_GPIO_Init(P2_8, &config);
	XMC_GPIO_Init(P2_9, &config);
	//14
  XMC_GPIO_Init(P14_7, &config);

	//P0_0 0_1 0_2 0_3 0_4 0_5
  config.mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT3;
  config.output_level = XMC_GPIO_OUTPUT_LEVEL_LOW;
  config.output_strength = XMC_GPIO_OUTPUT_STRENGTH_STRONG_SHARP_EDGE;		
  XMC_GPIO_Init(PWM_UH, &config);
	XMC_GPIO_Init(PWM_VH, &config);	
  XMC_GPIO_Init(PWM_WH, &config);	
	XMC_GPIO_Init(PWM_UL, &config);	
  XMC_GPIO_Init(PWM_VL, &config);	
	XMC_GPIO_Init(PWM_WL, &config);		
	
	//P14_6 14_9 14_14 
	config.mode = XMC_GPIO_MODE_INPUT_TRISTATE;
  XMC_GPIO_Init(R_ADC, &config);	
  XMC_GPIO_Init(I_U, &config);
  XMC_GPIO_Init(I_V, &config);
	
  //P2_2 2_5  串口通信
	
}
