#ifndef _POSIF_H_
#define _POSIF_H_

#include <XMC4400.h>

void POSIF0_Init(void);
void POSIF0_Start(void);
void Switch_on(void);
void Switch_off(void);

#endif
