#ifndef _PARAMETER_H_
#define _PARAMETER_H_

#include <XMC4400.h>                    
#define REC_LENGTH  			200  	//定义最大接收字节数
extern int16_t Angle,Angle_120,Angle_240;

/*****STATE*****/
typedef enum
{
	Motor_Stop,
	Motor_Lock,
	Motor_Start,
	Motor_Run,

}Motor_State;
extern Motor_State Motor_flag;


/*****SPEED*****/
#define SPEED_MAX (5600)
extern uint16_t Speed_buff;
extern uint16_t Speed;
extern uint32_t speed_time[8];
extern uint64_t speed_time_sum;
extern volatile uint8_t speed_count;
extern volatile uint8_t speed_closeloop;
extern uint8_t speed_get_flag;


/*****CURRENT*****/
extern int32_t Iq0;
extern int32_t Id0;
extern int16_t Iq;
extern int16_t Iq_Ref;
extern int16_t Id;
extern int16_t Id_Ref;
extern int32_t IUAC_mA;
extern int32_t IVAC_mA;
extern int32_t IWAC_mA;
extern uint8_t parament_choose;


/*****ADC*****/
extern uint16_t Sin_value;
extern uint16_t Cos_value;
extern uint16_t R_value;
extern uint16_t Angle_value;
extern uint16_t angle_value_in[11];
extern uint16_t r_value_in[8];
extern uint16_t angle_value;
extern uint16_t angle_value_last;
extern uint16_t angle_gap[10];
extern uint8_t angle_count;
extern uint8_t adc_count;
extern uint8_t adc_flag;

extern uint16_t sin_max;
extern uint16_t sin_min;
extern uint16_t cos_max;
extern uint16_t cos_min;
extern uint16_t sin_max_temp;
extern uint16_t sin_min_temp;
extern uint16_t cos_max_temp;
extern uint16_t cos_min_temp;
extern uint8_t  sincos_flag;
extern uint16_t sin_dc_component;
extern uint16_t cos_dc_component;
extern uint16_t IU_dc_component;
extern uint16_t IV_dc_component;
extern int16_t  Sin_Normalization;
extern int16_t  Cos_Normalization;
extern int16_t Angle_feedback;
extern int16_t Cosangle_feedback; 
extern uint16_t Angle_rotor;
extern uint16_t Angle_offset;
/*****UART*****/
/*****UART*****/
extern uint8_t Uart_Buffer[REC_LENGTH];//接收buffer
extern uint16_t Rec_Sta;
extern uint8_t Uart_Send_Flag;
extern uint8_t send_buf[32];   //发送buffer

extern uint8_t hand_shake_Flag;//握手
extern uint8_t uart_state_flag;

/*****PID*****/
extern uint16_t Speed_Ref;
extern uint16_t Speed_Cmd;
extern uint16_t Kp;
extern uint16_t Ki;
extern uint16_t Kd;
extern uint16_t AKp;
extern uint16_t AKi;
extern int32_t PID_output;
extern int16_t AIvalue_limit;
extern int16_t PID_Limit;
extern int32_t Ivalue_limit;
extern int16_t output_limit;
extern int16_t s_error1;
extern int16_t s_error2;
extern int16_t s_error3;
extern int32_t P_value;
extern int32_t I_value;
extern int32_t D_value;

extern int32_t APID_output;
extern int16_t APID_Limit;
extern int16_t Aoutput_limit;
extern int16_t a_error1;
extern int16_t a_error2;
extern int32_t AP_value;
extern int32_t AI_value;
extern int16_t Angle_feedback_t;

extern int32_t  Iq_PID_output;
extern int16_t  Iq_PID_Limit;
extern int32_t  Id_PID_output;
extern int16_t  Id_PID_Limit;
extern int16_t Iq_Error1;
extern int16_t Iq_Error2;
extern int32_t Iq_Pvalue;
extern int32_t Iq_Ivalue;
extern uint16_t Iq_Kp;
extern uint16_t Iq_Ki;
extern uint32_t Iq_Kp_temp;
extern uint32_t Iq_Ki_temp;

extern int16_t Id_Error1;
extern int16_t Id_Error2;
extern int32_t Id_Pvalue;
extern int32_t Id_Ivalue;
extern uint16_t Id_Kp;
extern uint16_t Id_Ki;

extern uint16_t Uq_Limit;


/*****clarke-park*****/
extern int16_t Ud;
extern int16_t Uq;
extern int32_t Ua0;
extern int32_t Ub0;
extern int16_t Ua;
extern int16_t Ub;

extern uint16_t aa;
extern uint16_t bb;

void Parameter_Init(void);
void PID_Init(void);

#endif
