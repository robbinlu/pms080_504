#include "Parameter.h"
#include "SVPWM.h"

int16_t Angle,Angle_120,Angle_240;
/*****STATE*****/
Motor_State Motor_flag;


/*****SPEED*****/
uint16_t Speed;
uint32_t speed_time[8];
uint64_t speed_time_sum;
uint8_t speed_get_flag;
volatile uint8_t speed_count;
volatile uint8_t speed_closeloop;

/*****CURRENT*****/
uint8_t adc_count = 0;

/*****ADC*****/
uint16_t R_value;

//sin cos信号与锁相环
uint16_t Sin_value;
uint16_t Cos_value;
uint16_t sin_max=2428;
uint16_t sin_min=1360;
uint16_t cos_max=2512;
uint16_t cos_min=1408;

uint16_t sin_dc_component=1894;
uint16_t cos_dc_component=1960;

int16_t  Sin_Normalization=0;
int16_t  Cos_Normalization=0;

int16_t Angle_feedback=0;
int16_t Angle_feedback_t=0;
int16_t Cosangle_feedback=0; 
uint16_t Angle_rotor=0;
uint16_t Angle_offset;


/*****UART*****/
/*****UART*****/
uint8_t Uart_Buffer[REC_LENGTH];//接收buffer
uint16_t Rec_Sta = 0;
uint8_t Uart_Send_Flag = 0;
uint8_t send_buf[32];   //发送buffer

uint8_t hand_shake_Flag = 0;//握手
uint8_t uart_state_flag = 0;

/*****PID*****/


uint16_t AKp = 50;
uint16_t AKi = 155;

uint16_t Uq_Limit = 20000;

int32_t APID_output;
int16_t AIvalue_limit;
int16_t APID_Limit;
int16_t a_error1=0;
int16_t a_error2=0;
int32_t AP_value;
int32_t AI_value;

/*****clarke-park*****/
int16_t Ud;
int16_t Uq;
int32_t Ua0;
int32_t Ub0;
int16_t Ua;
int16_t Ub;

//电流闭环
int32_t Iq0;
int32_t Id0;
int16_t Iq;
int16_t Iq_Ref;
int16_t Id;
int16_t Id_Ref;
int32_t IUAC_mA;
int32_t IVAC_mA;
int32_t IWAC_mA;

int32_t  Iq_PID_output;
int16_t  Iq_PID_Limit;
int32_t  Id_PID_output;
int16_t  Id_PID_Limit;

int16_t Iq_Error1;
int16_t Iq_Error2;
int32_t Iq_Pvalue;
int32_t Iq_Ivalue;
uint16_t Iq_Kp = 48;
uint16_t Iq_Ki = 20;

int16_t Id_Error1;
int16_t Id_Error2;
int32_t Id_Pvalue;
int32_t Id_Ivalue;

uint16_t IU_dc_component=2052;
uint16_t IV_dc_component=2048;

uint16_t aa = 0;
uint16_t bb = 0;

//转速闭环
uint16_t Speed_Ref;
uint16_t Speed_Cmd = 0;
uint16_t Kp;
uint16_t Ki;
uint16_t Kd;
int16_t s_error1;
int16_t s_error2;
int16_t s_error3;
int32_t P_value;
int32_t I_value;
int32_t D_value;
int32_t PID_output;
int16_t PID_Limit=10000;
int16_t output_limit=15000;


void Parameter_Init(void)
{
	//Uq由上位机给定，其余参数初始化，SVPWM周期
	
	SVPWM16.PWM_Period=3750;
	//Speed_Ref=0;	

	Speed=0;
	uint8_t reset_speed;
	for(reset_speed=0;reset_speed<8;reset_speed++)
	{
		speed_time[reset_speed]=0;
	}
	speed_time_sum = 0;
	speed_count=0;
	speed_get_flag=0;	
	
	Angle_offset=0;
	
	Ud=0;
	
	//电流环
	Id_Ref=0;

}

void PID_Init(void)
{
	//AKP AKI UQ上位机给
	AIvalue_limit=5000;
	APID_Limit=3600;
  APID_output=0;
  a_error1=0;
  a_error2=0;
  AP_value=0;
  AI_value=0;
	
	Id_PID_Limit=15000;
	Id_Error1=0;
	Id_Error2=0;
	
	Iq_PID_Limit=15000;
	Iq_Error1=0;
	Iq_Error2=0;
}

