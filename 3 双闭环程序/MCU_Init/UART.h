#ifndef UART_H_
#define UART_H_

#include <XMC4400.h>



void UART_Init(void);
void UART_SendData(uint16_t uwData);
void UART_SendBuffer(const uint8_t *Buffer, uint16_t length);


#endif
