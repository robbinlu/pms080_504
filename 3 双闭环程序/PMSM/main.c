#include <XMC4400.h>
#include "POSIF.h"
#include "UART.h"
#include "ADC.h"
#include "CCU4.h"
#include "CCU8.h"
#include "GPIO.h"
#include "FOC.h"
#include "Parameter.h"
#include "HALL_Function.h"
#include "DAC.h"

int main(void)
{
  	Parameter_Init();
 	  SysTick_Config(SystemCoreClock/1000);
		GPIO_Init();
	  UART_Init();
		ADC_Init();
		POSIF0_Init();
		CCU40_Init();	
		CCU80_Init();
		DAC_Init();
		CCU80_Start();
	  CCU40_CC41_Start();
	  CCU40_CC42_Start();
	
		while(1)
		{
			if(Uart_Send_Flag)
			{
				Uart_Send_Flag = 0;
				UART_SendBuffer(send_buf, 32);
			}
			Uart_Rec();
			
			if(Speed_Cmd<=100)
			{
				 Motor_flag=Motor_Stop;
				 Parameter_Init();
				 PID_Init();
			}
	
			if(Motor_flag==Motor_Stop&&Speed_Cmd>600)
			{
				 Motor_flag=Motor_Run;
			}
		}
}
