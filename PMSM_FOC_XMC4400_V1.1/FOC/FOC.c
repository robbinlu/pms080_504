#include "FOC.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "Table.h"
#include "DAC.h"
#include "Interrupt.h"

//pll
void Angle_PID(void)
{
	a_error2=a_error1;
  a_error1=(int16_t)(((Sin_Normalization*sintable16[Cosangle_feedback])-(Cos_Normalization*sintable16[Angle_feedback]))/1000);  
	AP_value=(int32_t)(AKp*(a_error1-a_error2));
	AI_value=(int32_t)(AKi*a_error1);
	APID_output=AP_value+AI_value;
	APID_output=APID_output>>15; //afasf
	if(APID_output>APID_Limit)
	{
		APID_output=APID_Limit;
	}
	if(APID_output<(0-APID_Limit))
	{
		APID_output=(0-APID_Limit);
	}
	Angle_feedback=(Angle_feedback+APID_output+3600)%3600;
	if(Angle_feedback>2700)                   //cos(a)=sin(a+90°)
	{
		Cosangle_feedback=Angle_feedback-2700;
	}
	else if(Angle_feedback<2700)
	{
		Cosangle_feedback=Angle_feedback+900;
	}
	else if(Angle_feedback==2700)
	{
		Cosangle_feedback=0;
	}
	Angle_rotor=(((Angle_feedback-Angle_offset+3600)%3600)<<2)%3600;
}

int16_t Speed_PID(void)
{
	s_error1=(int16_t)(Speed_Ref-Speed);
	if(Speed_Ref>=SPEED_MAX)
	{
		Speed_Ref=SPEED_MAX;
	}
	s_error3=s_error2;
	s_error2=s_error1;

	P_value=(int32_t)(Kp * (s_error1-s_error2));
	I_value=(int32_t)(Ki * s_error1);
	D_value=(int32_t)(Kd * (s_error1-2*s_error2+s_error3));
  
	if(I_value>Ivalue_limit)
	{
			I_value = Ivalue_limit;
	}
	if(I_value<(0-Ivalue_limit))
	{
		  I_value = (0-Ivalue_limit);
	}
	
	PID_output=P_value+I_value+D_value;

	PID_output=PID_output>>13;
	
	if(PID_output>PID_Limit)
	{
		PID_output=PID_Limit;
	}
	if(PID_output<(0-PID_Limit))
	{
		PID_output=(0-PID_Limit);
	}
	
	return (int16_t)PID_output;
	
}

int16_t Iq_current_PI(void)
{
	Iq_Error2=Iq_Error1;
	Iq_Error1=(int16_t)(Iq_Ref-Iq);
	
	Iq_Pvalue=(int32_t)(Iq_Kp * (Iq_Error1-Iq_Error2));
	Iq_Ivalue=(int32_t)(Iq_Ki * Iq_Error1);
	
	Iq_PID_output=Iq_Pvalue+Iq_Ivalue;
	Iq_PID_output=Iq_PID_output>>12;
	
	if(Iq_PID_output>Iq_PID_Limit)
	{
		Iq_PID_output=Iq_PID_Limit;
	}
	if(Iq_PID_output<(0-Iq_PID_Limit))
	{
		Iq_PID_output=(0-Iq_PID_Limit);
	}
	return (int16_t)Iq_PID_output;
}

int16_t Id_current_PI(void)
{
	Id_Error2=Id_Error1;
	Id_Error1=(int16_t)(Id_Ref-Id);
	
	Id_Pvalue=(int32_t)(Iq_Kp * (Id_Error1-Id_Error2));
	Id_Ivalue=(int32_t)(Iq_Ki * Id_Error1);
	
	Id_PID_output=Id_Pvalue+Id_Ivalue;
	Id_PID_output=Id_PID_output>>12;
	
	if(Id_PID_output>Id_PID_Limit)
	{
		Id_PID_output=Id_PID_Limit;
	}
	if(Id_PID_output<(0-Id_PID_Limit))
	{
		Id_PID_output=(0-Id_PID_Limit);
	}
	return (int16_t)Id_PID_output;
}

void Park_inverse(uint16_t theta_in)
{
	uint16_t costheta; 
	
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	Ua0=Ud*(sintable16[costheta])-Uq*(sintable16[theta_in]);
	Ub0=Ud*(sintable16[theta_in])+Uq*(sintable16[costheta]);
	Ua=Ua0>>15;
	Ub=Ub0>>15;
}


void Park_conversion(uint16_t theta_in)
{
	uint16_t theta_in120;
	uint16_t theta_in240;
	uint16_t costheta;
	uint16_t costheta120;
	uint16_t costheta240;
	theta_in120=(theta_in+1200)%3600;
	theta_in240=(theta_in+2400)%3600;
	Angle=theta_in;
	Angle_120=theta_in120;
	Angle_240=theta_in240;
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	if(theta_in120>2700)    //cos(a)=sin(a+90°)
	{
		costheta120=theta_in120-2700;
	}
	else if(theta_in120<2700)
	{
		costheta120=theta_in120+900;
	}
	else if(theta_in120==2700)
	{
		costheta120=0;
	}
	
	if(theta_in240>2700)    //cos(a)=sin(a+90°)
	{
		costheta240=theta_in240-2700;
	}
	else if(theta_in240<2700)
	{
		costheta240=theta_in240+900;
	}
	else if(theta_in240==2700)
	{
		costheta240=0;
	}
  Id0=((IUAC_mA*(sintable16[costheta])))+((IVAC_mA*(sintable16[costheta240])))+((IWAC_mA*(sintable16[costheta120])));
	Iq0=-(((IUAC_mA*(sintable16[theta_in])))+((IVAC_mA*(sintable16[theta_in240])))+((IWAC_mA*(sintable16[theta_in120]))));
	Id=Id0>>17;
	Iq=Iq0>>17;
	//DAC_Output(Id>>2);
}

void Speed_closeloop(void)
{
	  int16_t Iq_Ref_temp = 0;
		Iq_Ref=Iq_Ref+Speed_PID();
		if(Iq_Ref>output_limit)
		{
			Iq_Ref=output_limit;
		}
		if(Iq_Ref<(0-output_limit))
		{
			Iq_Ref=(int16_t)(0-output_limit);
		}
		Iq_Ref_temp = Iq_Ref;
		if(Iq_Ref_temp<0)
		{
			Iq_Ref_temp = -Iq_Ref_temp;
		}
		parament_choose = Iq_Ref_temp / 1000;
		switch(parament_choose)
		{
			case 1:
				Iq_Kp=400,Iq_Ki=100;
				break;
			case 2:
				Iq_Kp=500,Iq_Ki=150;
				break;
			case 3:
				Iq_Kp=550,Iq_Ki=250;
				break;
			case 4:
				Iq_Kp=600,Iq_Ki=280;
				break;
			case 5:
				Iq_Kp=700,Iq_Ki=300;
				break;
			case 6:
				Iq_Kp=800,Iq_Ki=350;
				break;
			case 7:
				Iq_Kp=850,Iq_Ki=400;
				break;
			case 8:
				Iq_Kp=900,Iq_Ki=420;
				break;
			case 9:
				Iq_Kp=1000,Iq_Ki=450;
				break;
			case 10:
				Iq_Kp=1100,Iq_Ki=500;
				break;
			case 11:
				Iq_Kp=1200,Iq_Ki=520;
				break;
			case 12:
				Iq_Kp=1200,Iq_Ki=550;
				break;
			case 13:
				Iq_Kp=1300,Iq_Ki=550;
				break;
			case 14:
				Iq_Kp=1300,Iq_Ki=600;
				break;
			case 15:
				Iq_Kp=1350,Iq_Ki=600;
				break;
			case 16:
				Iq_Kp=1500,Iq_Ki=650;
				break;
			case 17:
				Iq_Kp=1500,Iq_Ki=700;
				break;
			case 18:
				Iq_Kp=1550,Iq_Ki=700;
				break;
			case 19:
				Iq_Kp=1600,Iq_Ki=750;
				break;
			case 20:
				Iq_Kp=1700,Iq_Ki=800;
				break;
		}
}

void Current_closeloop(void)
{
		Park_conversion(Angle_rotor);        //在这个函数后面可以通过Iq实现Kp、Ki参数的分段。
		Uq=Uq+Iq_current_PI();
		Ud=Ud+Id_current_PI();
	 	if(Uq>32000)
		{
				Uq=32000;
		}
		if(Uq<-32000)
		{
				Uq=-32000;
		}
		if(Ud>32000)
		{
				Ud=32000;
		}
		if(Ud<-32000)
		{
				Ud=-32000;
		}
}

