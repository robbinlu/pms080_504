#include "Parameter.h"
#include "SVPWM.h"

int16_t Angle,Angle_120,Angle_240;
/*****STATE*****/
Motor_State Motor_flag;


/*****SPEED*****/
uint16_t Speed_buff;
uint16_t Speed;
uint32_t speed_time[8];
uint64_t speed_time_sum;
uint8_t speed_get_flag;
volatile uint8_t speed_count;
volatile uint8_t speed_closeloop;


/*****CURRENT*****/
int32_t Iq0;
int32_t Id0;
int16_t Iq;
int16_t Iq_Ref;
int16_t Id;
int16_t Id_Ref;
int32_t IUAC_mA;
int32_t IVAC_mA;
int32_t IWAC_mA;
uint8_t parament_choose;

/*****ADC*****/
uint16_t Sin_value;
uint16_t Cos_value;
uint16_t Angle_value=0;
uint16_t angle_value_in[11];
uint16_t angle_value;
uint16_t angle_value_last;
uint16_t angle_gap[10]={0};
uint8_t angle_count;
uint8_t angle_gap_count;


uint16_t sin_max=3150;
uint16_t sin_min=1750;
uint16_t cos_max=3150;
uint16_t cos_min=1750;
uint16_t sin_max_temp=0;
uint16_t sin_min_temp=3500;
uint16_t cos_max_temp=0;
uint16_t cos_min_temp=3500;
uint8_t  sincos_flag;
uint16_t sin_dc_component=2450;
uint16_t cos_dc_component=2450;
uint16_t IU_dc_component=2052;
uint16_t IV_dc_component=2048;

int16_t  Sin_Normalization=0;
int16_t  Cos_Normalization=0;
int16_t Angle_feedback=0;
int16_t Cosangle_feedback=0; 
uint16_t Angle_rotor=0;


/*****UART*****/
uint8_t Oscilloscope_flag;
uint32_t Oscilloscope_Data[4];
uint16_t var[8]; 
uint8_t Receive_data[128];
uint8_t start_position;
uint8_t mid1_position;
uint8_t mid2_position;
uint8_t mid3_position;
uint8_t mid4_position;
uint8_t mid5_position;
uint8_t end_position;
uint8_t cmdstart_position;
uint8_t cmdend_position;
uint8_t currentstart_position;
uint8_t currentend_position;
uint8_t speedstart_position;
uint8_t speedend_position;

/*****PID*****/
uint16_t Speed_Ref;
uint16_t Kp;
uint16_t Ki;
uint16_t Kd;
uint16_t AKp;
uint16_t AKi;
int32_t PID_output;
int16_t PID_Limit;
int16_t output_limit;
int16_t Ivalue_limit;
int16_t s_error1;
int16_t s_error2;
int16_t s_error3;
int32_t P_value;
int32_t I_value;
int32_t D_value;


int32_t APID_output;
int16_t AIvalue_limit;
int16_t APID_Limit;
int16_t Aoutput_limit;
int16_t a_error1;
int16_t a_error2;
int32_t AP_value;
int32_t AI_value;



int32_t  Iq_PID_output;
int16_t  Iq_PID_Limit;
int32_t  Id_PID_output;
int16_t  Id_PID_Limit;

int16_t Iq_Error1;
int16_t Iq_Error2;
int32_t Iq_Pvalue;
int32_t Iq_Ivalue;
uint16_t Iq_Kp;
uint16_t Iq_Ki;
uint32_t Iq_Kp_temp;
uint32_t Iq_Ki_temp;

int16_t Id_Error1;
int16_t Id_Error2;
int32_t Id_Pvalue;
int32_t Id_Ivalue;
uint16_t Id_Kp;
uint16_t Id_Ki;





/*****clarke-park*****/
int16_t Ud;
int16_t Uq;
int32_t Ua0;
int32_t Ub0;
int16_t Ua;
int16_t Ub;



void Parameter_Init(void)
{
	Motor_flag=Motor_Stop;
	SVPWM16.PWM_Period=3750;
	IU_dc_component=2052;
  IV_dc_component=2048;
	speed_count=0;
	angle_count=0;
	speed_get_flag=0;
	sincos_flag=0;
	Uq=0;
	Ud=0;
  Iq_Ref=0;
  Id_Ref=0;
  Speed_Ref=0;	
	Oscilloscope_flag=1;
	Speed=0;
}

void PID_Init(void)
{
//	Kp=0;                      //50000(350)     450000(490)
//	Ki=0;
//  Kd=0;	                     //30
	Ivalue_limit = 20000;
	PID_Limit=10000;           //100
	output_limit=32000;        //5000
	s_error1=0;
	s_error2=0;
	s_error3=0;
	
	AKp=50;                //45        64     64     128     256(0.5us)     45      128(62.6us)     400      锁相环的Kp和Ki加起来要为一个定值大约是600 
	AKi=500;                //8       768     32      20      24             512      256            200
	AIvalue_limit=5000;
	APID_Limit=3600;
	Aoutput_limit=32000;
	a_error1=0;
	a_error2=0;
	
	Id_Kp=0;               //2000      600
	Id_Ki=0;               //600       400
	Id_PID_Limit=32000;
	Id_Error1=0;
	Id_Error2=0;
	
//  Iq_Kp=400;
//	Iq_Ki=100;
	Iq_PID_Limit=32000;
	Iq_Error1=0;
	Iq_Error2=0;
}

