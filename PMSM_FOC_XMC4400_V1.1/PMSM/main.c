#include <XMC4400.h>
#include "POSIF.h"
#include "UART.h"
#include "ADC.h"
#include "CCU4.h"
#include "CCU8.h"
#include "GPIO.h"
#include "FOC.h"
#include "Parameter.h"
#include "HALL_Function.h"
#include "DAC.h"
#include "serial.h"
#include "shell.h"

int main(void)
{
  	Parameter_Init();
 	  SysTick_Config(SystemCoreClock/1000);
		GPIO_Init();
		serial_init();
		ADC_Init();
		POSIF0_Init();
		CCU40_Init();	
		CCU80_Init();
		DAC_Init();
		CCU80_Start();
	  CCU40_CC41_Start();
	  CCU40_CC42_Start();
		while(1)
		{
			if(Speed_buff==1)
			{
				 Parameter_Init();
				 PID_Init();
			}
			if(Motor_flag==Motor_Stop&&Speed_buff==2)
			{
				 Motor_flag=Motor_Run;                   //Uq=5000时，Iq大约为8000多,Uq=10000时，Iq大约为16000多
				 Iq_Ref = 3000;
			}
			if(Motor_flag==Motor_Run)
			{
				 if(Speed_buff==3)
				 {
					 Iq_Ref = 4000;
				 }
				 if(Speed_buff==4)
				 {
					 Iq_Ref = 5000;
				 }
				 if(Speed_buff==5)
				 {
					 Iq_Ref = 6000;
				 }
				 if(Speed_buff==6)
				 {
					 Iq_Ref = 7000;
				 }
				 if(Speed_buff==7)
				 {
					 Iq_Ref = 8000;
				 }
				 if(Speed_buff==8)
				 {
					 Iq_Ref = 9000;
				 }
			}
		}
}
