#include "FOC.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "Table.h"
#include "DAC.h"
#include "Interrupt.h"

static uint8_t send_cnt = 50;
void Angle_PID(void)
{
	a_error2=a_error1;
  a_error1=(int16_t)(((Sin_Normalization*sintable16[Cosangle_feedback])-(Cos_Normalization*sintable16[Angle_feedback]))/1000);  
	AP_value=(int32_t)(AKp*(a_error1-a_error2));
	AI_value=(int32_t)(AKi*a_error1);
	APID_output=AP_value+AI_value;
	APID_output=APID_output>>15;
	if(APID_output>APID_Limit)
	{
		APID_output=APID_Limit;
	}
	if(APID_output<(0-APID_Limit))
	{
		APID_output=(0-APID_Limit);
	}
	Angle_feedback=(Angle_feedback+APID_output+3600)%3600;
	if(Angle_feedback>2700)                   //cos(a)=sin(a+90°)
	{
		Cosangle_feedback=Angle_feedback-2700;
	}
	else if(Angle_feedback<2700)
	{
		Cosangle_feedback=Angle_feedback+900;
	}
	else if(Angle_feedback==2700)
	{
		Cosangle_feedback=0;
	}
	Angle_rotor=(((Angle_feedback-Angle_offset+3600)%3600)<<2)%3600;
}

int16_t Speed_PID(void)
{
	if(Speed_Ref>=SPEED_MAX)
	{
		Speed_Ref=SPEED_MAX;
	}
	
	s_error1=(int16_t)(Speed_Ref-Speed);

	P_value=(int32_t)(Kp * (s_error1-s_error2));
	I_value=(int32_t)(Ki * s_error1);
	D_value=(int32_t)(Kd * (s_error1-2*s_error2+s_error3));
  
//	if(I_value>Ivalue_limit)
//	{
//			I_value = Ivalue_limit;
//	}
//	if(I_value<(0-Ivalue_limit))
//	{
//			I_value = (0-Ivalue_limit);
//	}

	PID_output=P_value+I_value+D_value;

	PID_output=PID_output>>7;//13
	
	if(PID_output>PID_Limit)
	{
		PID_output=PID_Limit;
	}
	if(PID_output<(0-PID_Limit))
	{
		PID_output=(0-PID_Limit);
	}
	
	s_error3=s_error2;
	s_error2=s_error1;
	
	return (int16_t)PID_output;
	
}

int16_t Iq_current_PI(void)
{
	Iq_Error2=Iq_Error1;
	Iq_Error1=(int16_t)(Iq_Ref-Iq);
	
	Iq_Pvalue=(int32_t)(Iq_Kp * (Iq_Error1-Iq_Error2));
	Iq_Ivalue=(int32_t)(Iq_Ki * Iq_Error1);
	
	Iq_PID_output=Iq_Pvalue+Iq_Ivalue;
	Iq_PID_output=Iq_PID_output>>7;
	
	if(Iq_PID_output>Iq_PID_Limit)
	{
		Iq_PID_output=Iq_PID_Limit;
	}
	if(Iq_PID_output<(0-Iq_PID_Limit))
	{
		Iq_PID_output=(0-Iq_PID_Limit);
	}
	return (int16_t)Iq_PID_output;
}

int16_t Id_current_PI(void)
{
	Id_Error2=Id_Error1;
	Id_Error1=(int16_t)(Id_Ref-Id);
	
	Id_Pvalue=(int32_t)(Iq_Kp * (Id_Error1-Id_Error2));
	Id_Ivalue=(int32_t)(Iq_Ki * Id_Error1);
	
	Id_PID_output=Id_Pvalue+Id_Ivalue;
	Id_PID_output=Id_PID_output>>7;
	
	if(Id_PID_output>Id_PID_Limit)
	{
		Id_PID_output=Id_PID_Limit;
	}
	if(Id_PID_output<(0-Id_PID_Limit))
	{
		Id_PID_output=(0-Id_PID_Limit);
	}
	return (int16_t)Id_PID_output;
}

void Park_inverse(uint16_t theta_in)
{
	uint16_t costheta; 
	
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	Ua0=Ud*(sintable16[costheta])-Uq*(sintable16[theta_in]);
	Ub0=Ud*(sintable16[theta_in])+Uq*(sintable16[costheta]);
	Ua=Ua0>>15;
	Ub=Ub0>>15;
}


void Park_conversion(uint16_t theta_in)
{
	uint16_t theta_in120;
	uint16_t theta_in240;
	uint16_t costheta;
	uint16_t costheta120;
	uint16_t costheta240;
	theta_in120=(theta_in+1200)%3600;
	theta_in240=(theta_in+2400)%3600;
	Angle=theta_in;
	Angle_120=theta_in120;
	Angle_240=theta_in240;
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	if(theta_in120>2700)    //cos(a)=sin(a+90°)
	{
		costheta120=theta_in120-2700;
	}
	else if(theta_in120<2700)
	{
		costheta120=theta_in120+900;
	}
	else if(theta_in120==2700)
	{
		costheta120=0;
	}
	
	if(theta_in240>2700)    //cos(a)=sin(a+90°)
	{
		costheta240=theta_in240-2700;
	}
	else if(theta_in240<2700)
	{
		costheta240=theta_in240+900;
	}
	else if(theta_in240==2700)
	{
		costheta240=0;
	}
  Id0=((IUAC_mA*(sintable16[costheta])))+((IVAC_mA*(sintable16[costheta240])))+((IWAC_mA*(sintable16[costheta120])));
	Iq0=-(((IUAC_mA*(sintable16[theta_in])))+((IVAC_mA*(sintable16[theta_in240])))+((IWAC_mA*(sintable16[theta_in120]))));
	Id=Id0>>17;
	Iq=Iq0>>17;
	//DAC_Output(Id>>2);
}

void Speed_closeloop(void)
{
	  int16_t Iq_Ref_temp = 0;
		Iq_Ref=Iq_Ref+Speed_PID();
		if(Iq_Ref>output_limit)
		{
			Iq_Ref=output_limit;
		}
		if(Iq_Ref<(0-output_limit))
		{
			Iq_Ref=(int16_t)(0-output_limit);
		}
		Iq_Ref_temp = Iq_Ref;
		
		if(Iq_Ref_temp<0)
		{
			Iq_Ref_temp = -Iq_Ref_temp;
		}
				
		if(Iq_Ref_temp % 1000 > 980)
		{
			parament_choose = Iq_Ref_temp / 1000 + 1;
		}
		else
		{
			parament_choose = Iq_Ref_temp / 1000;
		}
		
		switch(parament_choose)
		{
			case 1:
				Iq_Kp=12,Iq_Ki=3;
//			  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 2:
				Iq_Kp=15,Iq_Ki=4;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 3:
				Iq_Kp=17,Iq_Ki=7;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 4:
				Iq_Kp=18,Iq_Ki=8;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 5:
				Iq_Kp=21,Iq_Ki=9;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 6:
				Iq_Kp=25,Iq_Ki=10;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 7:
				Iq_Kp=26,Iq_Ki=12;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 8:
				Iq_Kp=28,Iq_Ki=13;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 9:
				Iq_Kp=31,Iq_Ki=14;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 10:
				Iq_Kp=34,Iq_Ki=15;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 11:
				Iq_Kp=37,Iq_Ki=16;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 12:
				Iq_Kp=37,Iq_Ki=16;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 13:
				Iq_Kp=40,Iq_Ki=17;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 14:
				Iq_Kp=40,Iq_Ki=18;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 15:
				Iq_Kp=42,Iq_Ki=18;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 16:
				Iq_Kp=46,Iq_Ki=20;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 17:
				Iq_Kp=46,Iq_Ki=21;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 18:
				Iq_Kp=48,Iq_Ki=21;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 19:
				Iq_Kp=50,Iq_Ki=23;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
			case 20:
				Iq_Kp=53,Iq_Ki=25;
//						  Iq_Kp = Iq_Kp/32;
//			  Iq_Ki = Iq_Ki/32;
				break;
		}
}

void Current_closeloop(void)
{
		Park_conversion(Angle_rotor);        //在这个函数后面可以通过Iq实现Kp、Ki参数的分段。
		Uq=Uq+Iq_current_PI();
		Ud=Ud+Id_current_PI();
	 	if(Uq>Uq_Limit)
		{
				Uq=Uq_Limit;
		}
		if(Uq<-Uq_Limit)
		{
				Uq=-Uq_Limit;
		}
		if(Ud>Uq_Limit)
		{
				Ud=Uq_Limit;
		}
		if(Ud<-Uq_Limit)
		{
				Ud=-Uq_Limit;
		}
}

/*******************communication App*************************/
uint8_t CRC_check(uint8_t *Buf, uint8_t len)
{
	uint32_t sum=0;
	uint8_t i = 0;
	for(i = 0; i < len; i++)
	{
		sum += Buf[i];
	}
	return (uint8_t)(sum);
}


void Communication_App(void)
{
	if(--send_cnt == 0)
	{
		send_buf[0] = 0xa1;
		send_buf[1] = Motor_flag; 
		send_buf[2] = (uint8_t)Sin_value;
		send_buf[3] = (uint8_t)(Sin_value>>8);
		send_buf[4] = (uint8_t)Id;
		send_buf[5] = (uint8_t)(Id>>8);
		send_buf[6] = (uint8_t)Iq;
		send_buf[7] = (uint8_t)(Iq>>8);
		send_buf[8] = (uint8_t)Kp;
		send_buf[9] = (uint8_t)(Kp>>8);
		send_buf[10] = (uint8_t)Ki;
		send_buf[11] = (uint8_t)(Ki>>8);
		send_buf[12] = (uint8_t)Iq_Kp;
		send_buf[13] = (uint8_t)(Iq_Kp>>8);		
	  send_buf[14] = (uint8_t)Iq_Ki;
		send_buf[15] = (uint8_t)(Iq_Ki>>8);	
		send_buf[16] = (uint8_t)Speed_Ref;
		send_buf[17] = (uint8_t)(Speed_Ref>>8);
		send_buf[18] = (uint8_t)Uq_Limit;
		send_buf[19] = (uint8_t)(Uq_Limit>>8);
		send_buf[20] = (uint8_t)Cos_value;
		send_buf[21] = (uint8_t)(Cos_value>>8);
		send_buf[22] = 0;
		send_buf[23] = 0;
		send_buf[24] = 0;
		send_buf[25] = 0;
		send_buf[26] = 0;
		send_buf[27] = 0;
		send_buf[28] = 0;
		send_buf[29] = CRC_check(send_buf,29);
		send_buf[30] = 0x78;
		send_buf[31] = 0x56;
		Uart_Send_Flag = 1;
		send_cnt = 50;
	}
}

void Uart_Rec(void)
{
	int16_t data16 = 0;
	if(Rec_Sta&0x8000)
	{
		if(((Rec_Sta&0x3fff) == 6)&&(Uart_Buffer[0]==0xa1))//8-2
		{
			data16 = (int16_t)(Uart_Buffer[2])|(Uart_Buffer[3]<<8);
			switch(Uart_Buffer[1])
			{
				case 0: hand_shake_Flag = data16;
								break;//start/stop
				case 1: if(data16 > 1)
								{
									data16 = 0;
								}
								uart_state_flag = data16;
								break;//start/stop
				case 2: if(data16 > SPEED_MAX)
								{
									data16 = SPEED_MAX;
								}
								Speed_Cmd = data16;
                R_value = Speed_Cmd;
								break;
				case 3:	Kp = data16;
								break;
								
				case 4: Ki = data16;
								break;
								
   			case 5: Iq_Kp = data16;
						    break;
								
			  case 6: Iq_Ki = data16;
								break;
								
			  case 7: Uq_Limit = data16;
								break;		
								
				default:break;				
			}
		}	
		Rec_Sta = 0;
	}
}
