#include "ADC.h"

//sin,cos,ab相电流，电位器，两个温敏电阻
void ADC_Init(void)
{
	WR_REG(SCU_RESET->PRSET0, SCU_RESET_PRSET0_VADCRS_Msk, SCU_RESET_PRSET0_VADCRS_Pos, 1);
	WR_REG(SCU_RESET->PRCLR0, SCU_RESET_PRCLR0_VADCRS_Msk, SCU_RESET_PRCLR0_VADCRS_Pos, 1);	
	
	VADC->CLC	&=	0xfffffffe;
	while(VADC->CLC!=0){}								//wait for clock ok

	VADC->GLOBCFG=0x00008000;			//      ADC分频设置 
//	WR_REG(VADC->GLOBCFG, VADC_GLOBCFG_DPCAL0_Msk, VADC_GLOBCFG_DPCAL0_Pos, 1);                      //组0无后校准
//  WR_REG(VADC->GLOBCFG, VADC_GLOBCFG_DPCAL1_Msk, VADC_GLOBCFG_DPCAL1_Pos, 1);                        //组1无后校准
  WR_REG(VADC->GLOBCFG, VADC_GLOBCFG_SUCAL_Msk, VADC_GLOBCFG_SUCAL_Pos, 1);                          //启动初始校准阶段
		

	VADC_G0->CHASS	|=	0x41;
	VADC_G1->CHASS	|=	0x43;


	VADC_G0->ARBPR	|=0x0700088b;
	VADC_G1->ARBPR	|=0x0700088b;					//p1951
		
	VADC_G0->ARBCFG	|= 0x00000003;       

	while(VADC_G0->ARBCFG & 0x10000000){}
	

	VADC_G0->SYNCTR	|=	0x11;							//该内核为同步从内核
	VADC_G1->SYNCTR	|=	0;			          //该内核为同步主内核			

	
  VADC_G1->QINR0	|=	0xa1;	
	VADC_G1->QINR0	|=	0x20;					//Queue 0 input register,		P1959 在发出转换请求之前，有效队列条目等待一个触发事件发生。  无请求源中断  通道1
	VADC_G1->QINR0	|=	0x26;	

	VADC_G1->QCTRL0	|=	0x0000c800;			
													//trigger operation mode and external trigger level can be written

	VADC_G1->QMR0	|=	0x05;					//queue mode register, operation mode of queued request source   p1955    在所选触发输入信号 REQTR 的所选边沿产生触发事件。  使能门控 如果在队列 0 寄存器或备份寄存器中有被挂起的有效转换请求，发出转换请求 
													//enable gate,enable external trigger,

  VADC_G0->CHCTR[0]	|=	0x00000400;
	VADC_G0->CHCTR[6]	|=	0x00060400;
	VADC_G1->CHCTR[1]	|=	0x00010000;			//channel 1 result register 1, no limit check or broken wire check
  VADC_G1->CHCTR[0]	|=	0x00000400;
	VADC_G1->CHCTR[6]	|=	0x00060400;	

	VADC_G1->ARBCFG		= 0x00000003;			  //set master's ANONC bit at the end of the initialization P1951    正常工作模式 
	while(VADC_G1->ARBCFG & 0x10000000){}		//CAL completed   等待采样完成 
	  
	WR_REG(VADC_G1->RCR[1], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV0NP_Msk, VADC_G_REVNP0_REV1NP_Pos, 0);          //选择组1的0通道结果事件的服务请求线0

	NVIC_SetPriority(VADC0_G1_0_IRQn, 0);	
	NVIC_EnableIRQ(VADC0_G1_0_IRQn);
}

