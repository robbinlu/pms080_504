#include "UART.h"
#include <xmc_uart.h>
#include "xmc_gpio.h"


volatile unsigned int uwTemp;


/*****************************************************************************
*     Global Variable Definitions
*****************************************************************************/
uint16_t ReceData;

/*****************************************************************************
 *    Global Function Definitions
 *****************************************************************************/
void UART_Init(void);
void UART_SendData(uint16_t uwData);

const XMC_UART_CH_CONFIG_t uart_config =
{
  .baudrate = 115200,
  .data_bits = 8,
  .stop_bits = 1,
};


/*****************************************************************************
 * Function:      void UART_Init(void)
 * Description:   Initialize the UART
 *                TX: P2.5, RX: P2.2
 *                Full Duplex; No Parity; One stop bit;Baud Rate: 115200
 * Caveats:       2012-08-01 15:34
 *****************************************************************************/
 void UART_Init(void)
{
		XMC_UART_CH_Init(XMC_UART0_CH1, &uart_config);
	
    ///  - The data input DX0B is selected for pin 
    USIC0_CH1->DX0CR     =  0x0000;      // RX: P2.2

    ///  - USIC0_CH0 Interrupt Node Pointer Register:SR0
    USIC0_CH1->INPR = 0x0;                             

    ///  - LSB first; passive data level is 1.
    ///  - DX0 and DOUT0;  Word Length (WLE) = 7 (8 Bit)
    USIC0_CH1->SCTR = 0x07070102;

    ///  - TBUF Data Enable (TDEN) = 1;TBUF Data Single Shot Mode (TDSSM) = 1
    USIC0_CH1->TCSR     =  0x00000500;                   

    ///  - Sample Mode (SMD) = 1; 1 Stop bit is selected
    ///  - Sample Point (SP) = 7;  Pulse Length (PL) = 0
   	USIC0_CH1->PCR_ASCMode	   =  0x00000701;
   	
    ///  - TBIF is set to simplify polling
    USIC0_CH1->PSR_ASCMode      |=  0x2000;     

    ///  Configuration of the used USIC0_CH0 Interrupts:
	  NVIC_SetPriority(USIC0_0_IRQn, 3);
	  NVIC_EnableIRQ(USIC0_0_IRQn);

    ///  - ASC (SCI, UART) Protocol is selected 
    ///  - The parity generation is disabled
    ///  - Receive interrupt is enabled
    ///  - Alternate receive interrupt is enabled
    USIC0_CH1->CCR       =  0xC002;      

    ///  Configuration of the used USIC0_CH1 Port Pins:
    PORT2->IOCR4 |= 0x00009000;		// P2.5
		
} //  End of function UART_Init


/*****************************************************************************
 * Function:      void UART_SendData(uword uwData)
 * Description:   This function clears the transmit buffer Indication flag 
 *                first  & then writes the send data initialization word into 
 *                the transmit buffer register.
 * Caveats:       2012-08-01 15:34
 *****************************************************************************/
void UART_SendData(uint16_t uwData)
{
    USIC0_CH1->PSCR     |= 0x1000;   //  clear transmit buffer indication flag
    USIC0_CH1->TBUF[0]    = uwData;   //  load transmit buffer register

    while(!(USIC0_CH1->PSR_ASCMode & 0x1000));   //  wait until tx buffer indication flag is set

} //  End of function UART_SendData

void UART_SendBuffer(const uint8_t *Buffer, uint16_t length)
{
	while(length--)
	{
		UART_SendData(*Buffer++);
	}
}
