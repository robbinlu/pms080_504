#include "Interrupt.h"
#include "Parameter.h"
#include "UART.h"
#include "DAC.h"
#include "SVPWM.h"
#include "FOC.h"
#include "POSIF.h"
#include "HALL_Function.h"
#include "VCAN_computer.h"
#include <xmc_uart.h>
#include <xmc_gpio.h>

void USIC0_0_IRQHandler(void)
{
		uint8_t Res;
    if ((USIC0_CH1->PSR_ASCMode & 0x8000)||(USIC0_CH1->PSR_ASCMode & 0x4000))     // Alternate receive interrupt flag
    {
        USIC0_CH1->PSCR   |= 0x8000;         // clear PSR_AIF
				USIC0_CH1->PSCR   |= 0x4000;            // clear PSR_RIF   
				Res = USIC0_CH1->RBUF;	
				if((Rec_Sta&0x8000)==0)
				{
					if(Rec_Sta&0x4000)
					{
						if(Res!=0x12)
							Rec_Sta=0;//接收错误,重新开始
						else 
							Rec_Sta|=0x8000;	//接收完成 
					}
					else 
					{	
						if(Res==0x34)
							Rec_Sta|=0x4000;
						else
						{
							Uart_Buffer[Rec_Sta&0X3FFF]=Res ;
							Rec_Sta++;
							if(Rec_Sta>(REC_LENGTH-1))Rec_Sta=0;//接收数据错误,重新开始接收	  
						}		 
					}
				}
    }
}
void SysTick_Handler(void)
{
	if(hand_shake_Flag)
	{
		Communication_App();
	}
	if(Motor_flag==Motor_Run)
	{
			speed_closeloop++;
			if(speed_closeloop>=10)
			{
				 speed_closeloop=0;
				 Speed_closeloop();
			}
	}
}

void CCU40_1_IRQHandler(void)
{
	if((RD_REG(CCU40_CC41->INTS, CCU4_CC4_INTS_E0AS_Msk, CCU4_CC4_INTS_E0AS_Pos))==1)
	{
		if(Motor_flag==Motor_Run)
		{
			  sincos_flag=1;
		}
		Speed_Calculate();
		SET_BIT(CCU40_CC41->SWR, CCU4_CC4_SWR_RE0A_Pos); 
	}
}


void CCU80_0_IRQHandler(void)
{
	if((RD_REG(CCU80_CC80->INTS, CCU8_CC8_INTS_OMDS_Msk, CCU8_CC8_INTS_OMDS_Pos))==1)
	{
		switch(Motor_flag)
		{
			case Motor_Stop:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC81->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC82->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80->GCSS |=0x00000111;	
				break;
			}
			case Motor_Lock:
			{
				Park_inverse(0);
				SVPWM16_7(Ua,Ub);
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;			
				CCU80->GCSS |=0x00000111;
				break;
			}
			case Motor_Run:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
				CCU80->GCSS |=0x00000111;
				break;
			}
			default :
				break;
		}
		SET_BIT(CCU80_CC80->SWR, CCU8_CC8_SWR_ROM_Pos);		
	}
}

void VADC0_G1_0_IRQHandler(void)
{
		if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV0_Msk, VADC_G_REFLAG_REV0_Pos))==1)
		{
			Sin_value=(VADC_G1->RESD[0])&0x0000FFFF;
			Cos_value=(VADC_G0->RESD[0])&0x0000FFFF;
      if(Sin_value > sin_max_temp)	sin_max_temp = Sin_value;
			if(Sin_value < sin_min_temp)	sin_min_temp = Sin_value;
			if(Cos_value > cos_max_temp)	cos_max_temp = Cos_value;
			if(Cos_value < cos_min_temp)	cos_min_temp = Cos_value;
			if(sincos_flag==0)
			{
				sin_max = cos_max = 3150;
				sin_min = cos_min = 1750;
			}
			else
			{
				sin_max = sin_max_temp;
				cos_max = cos_max_temp;
				sin_min = sin_min_temp;
				cos_min = cos_min_temp;
			}
			sin_dc_component=(sin_max+sin_min)>>1;
			cos_dc_component=(cos_max+cos_min)>>1;
			Theta_Calculate();                 
			SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV0_Pos); 
		}
		if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV1_Msk, VADC_G_REFLAG_REV1_Pos))==1)
		{
			if(adc_count>7)
			{
				adc_count=0;
				adc_flag=1;
			}
			r_value_in[adc_count++] =(VADC_G1->RESD[1])&0x0000FFFF;
			if(adc_flag==1)
			{
					//R_value=((r_value_in[0]+r_value_in[1]+r_value_in[2]+r_value_in[3]+r_value_in[4]+r_value_in[5]+r_value_in[6]+r_value_in[7])>>3);
			}
			SET_BIT(VADC_G1->REFCLR, VADC_G_REFCLR_REV1_Pos); 
		}
		if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV6_Msk, VADC_G_REFLAG_REV6_Pos))==1)
		{
			IUAC_mA=(((VADC_G1->RESD[6])&0x0000FFFF)-IU_dc_component)*92;                         //原来是乘以128的，现在转换为mA单位，乘以92
			IVAC_mA=(((VADC_G0->RESD[6])&0x0000FFFF)-IV_dc_component)*92;
			IWAC_mA=-IUAC_mA-IVAC_mA;
			if(Motor_flag==Motor_Run)
			{
					Current_closeloop();
					Park_inverse(Angle_rotor);
					SVPWM16_7(Ua,Ub);
			}
			SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV6_Pos); 
		}
}
