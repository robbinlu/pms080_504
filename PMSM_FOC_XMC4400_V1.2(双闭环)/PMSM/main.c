#include <XMC4400.h>
#include "POSIF.h"
#include "UART.h"
#include "ADC.h"
#include "CCU4.h"
#include "CCU8.h"
#include "GPIO.h"
#include "FOC.h"
#include "Parameter.h"
#include "HALL_Function.h"
#include "DAC.h"

int main(void)
{
  	Parameter_Init();
 	  SysTick_Config(SystemCoreClock/1000);
		GPIO_Init();
	  UART_Init();
		ADC_Init();
		POSIF0_Init();
		CCU40_Init();	
		CCU80_Init();
		//DAC_Init();
		CCU80_Start();
	  CCU40_CC41_Start();
	  CCU40_CC42_Start();
		while(1)
		{
			if(Uart_Send_Flag)
			{
				Uart_Send_Flag = 0;
				UART_SendBuffer(send_buf, 32);
			}
			Uart_Rec();
			
			if(R_value<=100)
			{
				 Parameter_Init();
				 PID_Init();
			}
			if(Motor_flag==Motor_Stop&&R_value>600)
			{
				 Motor_flag=Motor_Run;
			}
			if(Motor_flag==Motor_Run)
			{
				 if(R_value>=600&&R_value<1000)
				 {
					 Speed_Ref = 750;                      //启动速度为750时，启动较为平稳
				 }
				 else if(R_value>=1000&&R_value<1400)
				 {
					 Speed_Ref = 1000;
				 }
				 else if(R_value>=1400&&R_value<1800)
				 {
					 Speed_Ref = 1500;
				 }
				 else if(R_value>=1800&&R_value<2200)
				 {
					 Speed_Ref = 1900;
				 }
				 else if(R_value>=2200&&R_value<2600)
				 {
					 Speed_Ref = 2300;
				 }
			}
		}
}
