#include "FOC.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "Table.h"
#include "DAC.h"
#include "Interrupt.h"

void Angle_PID(void)
{
	a_error2=a_error1;
  a_error1=(int16_t)(((Sin_Normalization*sintable16[Cosangle_feedback])-(Cos_Normalization*sintable16[Angle_feedback]))/1000);  
	AP_value=(int32_t)(AKp*(a_error1-a_error2));
	AI_value=(int32_t)(AKi*a_error1);
	APID_output=AP_value+AI_value;
	APID_output=APID_output>>15;
	if(APID_output>APID_Limit)
	{
		APID_output=APID_Limit;
	}
	if(APID_output<(0-APID_Limit))
	{
		APID_output=(0-APID_Limit);
	}
	Angle_feedback=(Angle_feedback+APID_output+3600)%3600;
	if(Angle_feedback>2700)                   //cos(a)=sin(a+90°)
	{
		Cosangle_feedback=Angle_feedback-2700;
	}
	else if(Angle_feedback<2700)
	{
		Cosangle_feedback=Angle_feedback+900;
	}
	else if(Angle_feedback==2700)
	{
		Cosangle_feedback=0;
	}
	Angle_rotor=(((Angle_feedback-Angle_offset+3600)%3600)<<2)%3600;
}

int16_t Speed_PID(void)
{
	s_error3=s_error2;
	s_error2=s_error1;
	
	if(Speed_Ref>=SPEED_MAX)
	{
		Speed_Ref=SPEED_MAX;
	}
	s_error1=(int16_t)(Speed_Ref-Speed);

	P_value=(int32_t)(Kp * (s_error1-s_error2));
	I_value=(int32_t)(Ki * s_error1);
	D_value=(int32_t)(Kd * (s_error1-2*s_error2+s_error3));

	PID_output=P_value+I_value+D_value;

	PID_output=PID_output>>13;
	
	if(PID_output>PID_Limit)
	{
		PID_output=PID_Limit;
	}
	if(PID_output<(0-PID_Limit))
	{
		PID_output=(0-PID_Limit);
	}
	
	return (int16_t)PID_output;
	
}

int16_t Iq_current_PI(void)
{
	Iq_Error2=Iq_Error1;
	Iq_Error1=(int16_t)(Iq_Ref-Iq);
	
	Iq_Pvalue=(int32_t)(Iq_Kp * (Iq_Error1-Iq_Error2));
	Iq_Ivalue=(int32_t)(Iq_Ki * Iq_Error1);
	
	Iq_PID_output=Iq_Pvalue+Iq_Ivalue;
	Iq_PID_output=Iq_PID_output>>12;
	
	if(Iq_PID_output>Iq_PID_Limit)
	{
		Iq_PID_output=Iq_PID_Limit;
	}
	if(Iq_PID_output<(0-Iq_PID_Limit))
	{
		Iq_PID_output=(0-Iq_PID_Limit);
	}
	return (int16_t)Iq_PID_output;
}

int16_t Id_current_PI(void)
{
	Id_Error2=Id_Error1;
	Id_Error1=(int16_t)(Id_Ref-Id);
	
	Id_Pvalue=(int32_t)(Iq_Kp * (Id_Error1-Id_Error2));
	Id_Ivalue=(int32_t)(Iq_Ki * Id_Error1);
	
	Id_PID_output=Id_Pvalue+Id_Ivalue;
	Id_PID_output=Id_PID_output>>12;
	
	if(Id_PID_output>Id_PID_Limit)
	{
		Id_PID_output=Id_PID_Limit;
	}
	if(Id_PID_output<(0-Id_PID_Limit))
	{
		Id_PID_output=(0-Id_PID_Limit);
	}
	return (int16_t)Id_PID_output;
}

void Park_inverse(uint16_t theta_in)
{
	uint16_t costheta; 
	
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	Ua0=Ud*(sintable16[costheta])-Uq*(sintable16[theta_in]);
	Ub0=Ud*(sintable16[theta_in])+Uq*(sintable16[costheta]);
	Ua=Ua0>>15;
	Ub=Ub0>>15;
}


void Park_conversion(uint16_t theta_in)
{
	uint16_t theta_in120;
	uint16_t theta_in240;
	uint16_t costheta;
	uint16_t costheta120;
	uint16_t costheta240;
	theta_in120=(theta_in+1200)%3600;
	theta_in240=(theta_in+2400)%3600;
	Angle=theta_in;
	Angle_120=theta_in120;
	Angle_240=theta_in240;
	if(theta_in>2700)    //cos(a)=sin(a+90°)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	
	if(theta_in120>2700)    //cos(a)=sin(a+90°)
	{
		costheta120=theta_in120-2700;
	}
	else if(theta_in120<2700)
	{
		costheta120=theta_in120+900;
	}
	else if(theta_in120==2700)
	{
		costheta120=0;
	}
	
	if(theta_in240>2700)    //cos(a)=sin(a+90°)
	{
		costheta240=theta_in240-2700;
	}
	else if(theta_in240<2700)
	{
		costheta240=theta_in240+900;
	}
	else if(theta_in240==2700)
	{
		costheta240=0;
	}
  Id0=((IUAC_mA*(sintable16[costheta]))>>2)+((IVAC_mA*(sintable16[costheta240]))>>2)+((IWAC_mA*(sintable16[costheta120]))>>2);
	Iq0=-(((IUAC_mA*(sintable16[theta_in]))>>2)+((IVAC_mA*(sintable16[theta_in240]))>>2)+((IWAC_mA*(sintable16[theta_in120]))>>2));
	Id=Id0>>15;
	Iq=Iq0>>15;
	if(Iq>=0&&Iq<=20000)
	{
		Iq_Kp_temp = 65*Iq + 387100;
		Iq_Ki_temp = 32*Iq + 140000;
		Iq_Kp_temp = Iq_Kp_temp / 1000;
		Iq_Ki_temp = Iq_Kp_temp / 1000;
		Iq_Kp = Iq_Kp_temp;
		Iq_Ki = Iq_Ki_temp;
	}
	//DAC_Output(Id>>2);
}

void Speed_closeloop(void)
{
	Iq_Ref=Iq_Ref+Speed_PID();
	if(Iq_Ref>output_limit)
	{
		Iq_Ref=output_limit;
	}
	if(Iq_Ref<(0-output_limit))
	{
		Iq_Ref=(int16_t)(0-output_limit);
	}
	if(Iq_Ref>32000)
	{
			Iq_Ref=32000;
	}
	if(Iq_Ref<-32000)
	{
		  Iq_Ref=-32000;
	}
}

void Current_closeloop(void)
{
		Park_conversion(Angle_rotor);        //在这个函数后面可以通过Iq实现Kp、Ki参数的分段。
		Uq=Uq+Iq_current_PI();
		Ud=Ud+Id_current_PI();
	 	if(Uq>32000)
		{
				Uq=32000;
		}
		if(Uq<-32000)
		{
				Uq=-32000;
		}
		if(Ud>32000)
		{
				Ud=32000;
		}
		if(Ud<-32000)
		{
				Ud=-32000;
		}
}

