#ifndef _FOC_H_
#define _FOC_H_


#include <XMC4400.h>


void Park_inverse(uint16_t theta_in);
void Park_conversion(uint16_t theta_in);
int16_t Speed_PID(void);
int16_t Iq_current_PI(void);
int16_t Id_current_PI(void);
void Angle_PID(void);
void Speed_closeloop(void);
void Current_closeloop(void);
#endif
