#include "CCU4.h"
void CCU40_Init(void)
{
	WR_REG(SCU_RESET->PRSET0, SCU_RESET_PRSET0_CCU40RS_Msk, SCU_RESET_PRSET0_CCU40RS_Pos, 1);
	WR_REG(SCU_RESET->PRCLR0, SCU_RESET_PRCLR0_CCU40RS_Msk, SCU_RESET_PRCLR0_CCU40RS_Pos, 1);	
	WR_REG(SCU_CLK->CLKSET, SCU_CLK_CLKSET_CCUCEN_Msk, SCU_CLK_CLKSET_CCUCEN_Pos, 1);		
	
	WR_REG(CCU40->GIDLC, CCU4_GIDLC_SPRB_Msk, CCU4_GIDLC_SPRB_Pos, 1);	
	
	//CC40
	WR_REG(CCU40_CC40->PSC, CCU4_CC4_PSC_PSIV_Msk, CCU4_CC4_PSC_PSIV_Pos, 0);
	WR_REG(CCU40_CC40->FPC, CCU4_CC4_FPC_PVAL_Msk, CCU4_CC4_FPC_PVAL_Pos, 0);
	WR_REG(CCU40_CC40->PRS, CCU4_CC4_PRS_PRS_Msk, CCU4_CC4_PRS_PRS_Pos, 7500);	
	WR_REG(CCU40_CC40->CRS, CCU4_CC4_CRS_CRS_Msk, CCU4_CC4_CRS_CRS_Pos, 7500);		
	WR_REG(CCU40_CC40->INTE, CCU4_CC4_INTE_PME_Msk, CCU4_CC4_INTE_PME_Pos, 1);
	WR_REG(CCU40_CC40->SRS, CCU4_CC4_SRS_POSR_Msk, CCU4_CC4_SRS_POSR_Pos, 0);	
	NVIC_SetPriority(CCU40_0_IRQn, 1);				  
	NVIC_EnableIRQ(CCU40_0_IRQn);
	
	
	WR_REG(CCU40_CC41->PSC, CCU4_CC4_PSC_PSIV_Msk, CCU4_CC4_PSC_PSIV_Pos, 6);
	WR_REG(CCU40_CC41->FPC, CCU4_CC4_FPC_PVAL_Msk, CCU4_CC4_FPC_PVAL_Pos, 6);
	WR_REG(CCU40_CC41->INS, CCU4_CC4_INS_EV0IS_Msk, CCU4_CC4_INS_EV0IS_Pos, 0);
	WR_REG(CCU40_CC41->INS, CCU4_CC4_INS_EV0EM_Msk, CCU4_CC4_INS_EV0EM_Pos, 1);
	WR_REG(CCU40_CC41->CMC, CCU4_CC4_CMC_CAP0S_Msk, CCU4_CC4_CMC_CAP0S_Pos, 1);
	WR_REG(CCU40_CC41->TC, CCU4_CC4_TC_CAPC_Msk, CCU4_CC4_TC_CAPC_Pos, 3);
	WR_REG(CCU40_CC41->TC, CCU4_CC4_TC_CCS_Msk, CCU4_CC4_TC_CCS_Pos, 1);
	WR_REG(CCU40_CC41->PRS, CCU4_CC4_PRS_PRS_Msk, CCU4_CC4_PRS_PRS_Pos, 0xFFFF);	
	WR_REG(CCU40_CC41->CRS, CCU4_CC4_CRS_CRS_Msk, CCU4_CC4_CRS_CRS_Pos, 0xFFFF);
  WR_REG(CCU40_CC42->CMC, CCU4_CC4_CMC_TCE_Msk, CCU4_CC4_CMC_TCE_Pos, 1);	
	WR_REG(CCU40_CC41->INTE, CCU4_CC4_INTE_E0AE_Msk, CCU4_CC4_INTE_E0AE_Pos, 1);		
	WR_REG(CCU40_CC41->SRS, CCU4_CC4_SRS_E0SR_Msk, CCU4_CC4_SRS_E0SR_Pos, 1);
	
	NVIC_SetPriority(CCU40_1_IRQn, 1);				  
	NVIC_EnableIRQ(CCU40_1_IRQn);

	WR_REG(CCU40_CC42->PSC, CCU4_CC4_PSC_PSIV_Msk, CCU4_CC4_PSC_PSIV_Pos, 6);
	WR_REG(CCU40_CC42->FPC, CCU4_CC4_FPC_PVAL_Msk, CCU4_CC4_FPC_PVAL_Pos, 6);
	WR_REG(CCU40_CC42->INS, CCU4_CC4_INS_EV0IS_Msk, CCU4_CC4_INS_EV0IS_Pos, 0);
	WR_REG(CCU40_CC42->INS, CCU4_CC4_INS_EV0EM_Msk, CCU4_CC4_INS_EV0EM_Pos, 1);
	WR_REG(CCU40_CC42->CMC, CCU4_CC4_CMC_CAP0S_Msk, CCU4_CC4_CMC_CAP0S_Pos, 1);
	WR_REG(CCU40_CC42->TC, CCU4_CC4_TC_CAPC_Msk, CCU4_CC4_TC_CAPC_Pos, 3);
	WR_REG(CCU40_CC42->TC, CCU4_CC4_TC_CCS_Msk, CCU4_CC4_TC_CCS_Pos, 1);
	WR_REG(CCU40_CC42->PRS, CCU4_CC4_PRS_PRS_Msk, CCU4_CC4_PRS_PRS_Pos, 0xFFFF);	
	WR_REG(CCU40_CC42->CRS, CCU4_CC4_CRS_CRS_Msk, CCU4_CC4_CRS_CRS_Pos, 0xFFFF);	
	
	
	//CC43    1us
	WR_REG(CCU40_CC43->PSC, CCU4_CC4_PSC_PSIV_Msk, CCU4_CC4_PSC_PSIV_Pos, 0);
	WR_REG(CCU40_CC43->FPC, CCU4_CC4_FPC_PVAL_Msk, CCU4_CC4_FPC_PVAL_Pos, 0);
	WR_REG(CCU40_CC43->PRS, CCU4_CC4_PRS_PRS_Msk, CCU4_CC4_PRS_PRS_Pos, 120);	
	WR_REG(CCU40_CC43->CRS, CCU4_CC4_CRS_CRS_Msk, CCU4_CC4_CRS_CRS_Pos, 120);		
	WR_REG(CCU40_CC43->INTE, CCU4_CC4_INTE_PME_Msk, CCU4_CC4_INTE_PME_Pos, 1);
	WR_REG(CCU40_CC43->SRS, CCU4_CC4_SRS_POSR_Msk, CCU4_CC4_SRS_POSR_Pos, 3);	
	NVIC_SetPriority(CCU40_3_IRQn, 1);				  
	NVIC_EnableIRQ(CCU40_3_IRQn);
	
	
	WR_REG(CCU40->GCSS, CCU4_GCSS_S0SE_Msk, CCU4_GCSS_S0SE_Pos, 1);	
	WR_REG(CCU40->GCSS, CCU4_GCSS_S1SE_Msk, CCU4_GCSS_S1SE_Pos, 1);		
	WR_REG(CCU40->GCSS, CCU4_GCSS_S2SE_Msk, CCU4_GCSS_S2SE_Pos, 1);		
	WR_REG(CCU40->GCSS, CCU4_GCSS_S3SE_Msk, CCU4_GCSS_S3SE_Pos, 1);	
	WR_REG(CCU40->GIDLC, CCU4_GIDLC_CS0I_Msk, CCU4_GIDLC_CS0I_Pos, 1);	
	WR_REG(CCU40->GIDLC, CCU4_GIDLC_CS1I_Msk, CCU4_GIDLC_CS1I_Pos, 1);	
	WR_REG(CCU40->GIDLC, CCU4_GIDLC_CS2I_Msk, CCU4_GIDLC_CS2I_Pos, 1);		
	WR_REG(CCU40->GIDLC, CCU4_GIDLC_CS3I_Msk, CCU4_GIDLC_CS3I_Pos, 1);		
	
}


void CCU40_CC40_Start(void)
{
	WR_REG(CCU40_CC40->TCSET, CCU4_CC4_TCSET_TRBS_Msk, CCU4_CC4_TCSET_TRBS_Pos, 1);	
}

void CCU40_CC41_Start(void)
{
	WR_REG(CCU40_CC41->TCSET, CCU4_CC4_TCSET_TRBS_Msk, CCU4_CC4_TCSET_TRBS_Pos, 1);	
}


void CCU40_CC42_Start(void)
{
	WR_REG(CCU40_CC42->TCSET, CCU4_CC4_TCSET_TRBS_Msk, CCU4_CC4_TCSET_TRBS_Pos, 1);	
}


void CCU40_CC43_Start(void)
{
	
	WR_REG(CCU40_CC43->TCSET, CCU4_CC4_TCSET_TRBS_Msk, CCU4_CC4_TCSET_TRBS_Pos, 1);	
}


