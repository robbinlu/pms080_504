#include "Parameter.h"
#include "SVPWM.h"

int16_t Angle,Angle_120,Angle_240;
/*****STATE*****/
Motor_State Motor_flag;


/*****POSIF*****/

uint8_t Halps_F[] ={0x19,0x32,0x13,0x2C,0x0D,0x26};//6-4-5-1-3-2
uint8_t Halps_B[] ={0x29,0x1A,0x0B,0x34,0x25,0x16};//2-3-1-5-4-6
uint8_t HALL_NOW_IN;
uint8_t HALL_count;
uint16_t Mcsms[] = {0x210,0x012,0x102,0x120,0x021,0x201};


/*****START*****/
uint16_t Lock_count;
uint16_t Start_count;
uint16_t Openloop_count;
uint16_t Openloop_step;


/*****SPEED*****/
uint16_t Speed_buff;
uint16_t Speed;
uint32_t speed_time[12];
uint64_t speed_time_sum;
uint8_t speed_get_flag;
volatile uint8_t speed_count;
volatile uint8_t speed_closeloop;


/*****CURRENT*****/
int32_t Iq0;
int32_t Id0;
int16_t Iq;
int16_t Iq_Ref;
int16_t Iq_Ref0;
int16_t Id;
int16_t Id_Ref;
uint16_t IU_value;
uint16_t IV_value;
int16_t IUAC_value;
int16_t IVAC_value;
int32_t IUAC_mA;
int32_t IVAC_mA;
int32_t IWAC_mA;
int32_t IUAC_value_in[8];
int32_t IVAC_value_in[8];
int32_t IWAC_value_in[8];
uint8_t IUAC_count=0;
uint8_t IVAC_count=0;


/*****ADC*****/
uint8_t adc_flag;
uint16_t R_value;
uint16_t Sin_value;
uint16_t Cos_value;
uint16_t Angle_value=0;
uint16_t r_value_in[8];
uint16_t sin_value_in[8];
uint16_t cos_value_in[8];
uint16_t angle_value_in[11];
uint16_t angle_value;
uint16_t angle_value_last;
uint16_t angle_gap[10]={0};
uint16_t adc_count;
uint8_t angle_count;
uint8_t angle_gap_count;
uint16_t sin_adc_count;
uint16_t cos_adc_count;
uint32_t sincos_count;
int16_t IU_offset;
int16_t IV_offset;

uint16_t sin_sum=0;
uint16_t cos_sum=0;
uint16_t r_sum=0;

uint16_t sin_max=3150;
uint16_t sin_min=1750;
uint16_t cos_max=3150;
uint16_t cos_min=1750;
uint16_t sin_max_temp=0;
uint16_t sin_min_temp=3500;
uint16_t cos_max_temp=0;
uint16_t cos_min_temp=3500;
uint16_t sin_max_temp_last=0;
uint16_t cos_max_temp_last=0;
uint16_t sin_min_temp_last=0;
uint16_t cos_min_temp_last=0;
uint8_t  sincos_flag=0;
uint16_t IU_max;
uint16_t IU_min;
uint16_t IV_max;
uint16_t IV_min;
uint16_t sin_dc_component=2450;
uint16_t cos_dc_component=2450;
uint16_t IU_dc_component=2052;
uint16_t IV_dc_component=2048;

int16_t  Sin_Normalization=0;
int16_t  Cos_Normalization=0;
int16_t  Angle_temple=0;
int16_t Angle_feedback=0;
int16_t Cosangle_feedback=0; 
uint16_t Angle_rotor=0;
uint8_t sin_threshold_flag=0;
uint8_t cos_threshold_flag=0;
uint8_t IU_threshold_flag=0;
uint8_t IV_threshold_flag=0;

//test
int16_t Angle_feedback1=0;
int16_t Angle_feedback2=0;


/*****UART*****/
uint8_t Oscilloscope_flag;
uint32_t Oscilloscope_Data[4];
uint8_t Uart_count;
uint32_t Buff_head_last;
uint32_t Buff_lenth_once;
uint8_t Receive_data[128];
uint8_t start_position;
uint8_t mid_position;
uint8_t end_position;
uint8_t cmdstart_position;
uint8_t cmdend_position;


uint16_t var[8]; 

/*****PID*****/
uint16_t Speed_Ref;
uint16_t Kp;
uint16_t Ki;
uint16_t Kd;
uint16_t AKp;
uint16_t AKi;
volatile uint16_t PID_count;
volatile uint8_t Speedtime_count;
volatile uint16_t APID_count;
int32_t PID_output;
int16_t PID_Limit;
uint16_t PID_Period;
int16_t output_limit;
int16_t s_error1;
int16_t s_error2;
int16_t s_error3;
int32_t P_value;
int32_t I_value;
int32_t D_value;

uint16_t c1,c2,c3;

int32_t APID_output;
int16_t AIvalue_limit;
int16_t APID_Limit;
int16_t Aoutput_limit;
int16_t a_error1;
int16_t a_error2;
int32_t AP_value;
int32_t AI_value;

uint32_t a_error=1000;


volatile uint16_t CPID_count;
int32_t  Iq_PID_output;
int16_t  Iq_PID_Limit;
int32_t  Id_PID_output;
int16_t  Id_PID_Limit;

int16_t Iq_Error1;
int16_t Iq_Error2;
int32_t Iq_Pvalue;
int32_t Iq_Ivalue;
uint16_t Iq_Kp;
uint16_t Iq_Ki;
uint16_t Iq_Kp_temp;
uint16_t Iq_Ki_temp;

int16_t Id_Error1;
int16_t Id_Error2;
int32_t Id_Pvalue;
int32_t Id_Ivalue;
uint16_t Id_Kp;
uint16_t Id_Ki;



/*****HALLFOC*****/
uint16_t theta_step;
uint16_t theta_count;
volatile uint16_t step_count;
uint16_t theta_offset;
uint16_t theta_count;
volatile uint16_t theta_gap;


/*****clarke-park*****/
uint16_t theta;
int16_t theta_test;
uint16_t theta_start;
int16_t Ud;
int16_t Uq;
int32_t Ua0;
int32_t Ub0;
int16_t Ua;
int16_t Ub;



void Parameter_Init(void)
{
	Motor_flag=Motor_Stop;
	SVPWM16.PWM_Period=3750;
	IU_dc_component=2050;
  IV_dc_component=2050;
	Lock_count=0;
	Openloop_count=0;
	Start_count=0;
	speed_count=0;
	Speedtime_count=0;
	angle_count=0;
	sincos_count=0;
	sin_adc_count=0;
	cos_adc_count=0;
	speed_get_flag=0;
	sincos_flag=0;
	Openloop_step=0;
	Buff_head_last=0;
	Buff_lenth_once=0;
	Uq=0;
	Ud=0;
  Iq_Ref=0;
  Id_Ref=0;
  Speed_Ref=0;	
	theta=0;
	Oscilloscope_flag=1;
  adc_flag=0;
	Speed=0;
}

void PID_Init(void)
{
	Kp=45000;                  //50000(350)     450000(490)
	Ki=490;
  Kd=0;	                     //30
	PID_count=0;
	PID_Limit=10000;          //100
	output_limit=32000;     //5000
	s_error1=0;
	s_error2=0;
	s_error3=0;
	
	AKp=50;                //45        64     64     128     256(0.5us)     45      128(62.6us)     400      锁相环的Kp和Ki加起来要为一个定值大约是600 
	AKi=500;                //8       768     32      20      24             512      256            200
	AIvalue_limit=5000;
	APID_Limit=3600;
	Aoutput_limit=32000;
	a_error1=0;
	a_error2=0;
	
	Id_Kp=0;               //2000      600
	Id_Ki=0;               //600       400
	CPID_count=0;
	Id_PID_Limit=32000;
	Id_Error1=0;
	Id_Error2=0;
	

	Iq_PID_Limit=32000;
	Iq_Error1=0;
	Iq_Error2=0;
}

