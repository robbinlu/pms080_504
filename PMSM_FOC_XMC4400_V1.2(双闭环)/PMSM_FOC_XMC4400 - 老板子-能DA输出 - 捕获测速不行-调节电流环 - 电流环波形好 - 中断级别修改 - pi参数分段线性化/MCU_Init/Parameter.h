#ifndef _PARAMETER_H_
#define _PARAMETER_H_

#include <XMC4400.h>
#define Angle_offset       1095                       //1095      1200
/*****RISING-FALLING*****/
#define RISING (1)

/*****FORWARD-BACKWARD*****/
#define FORWARD (0)

/*****DIRCTION*****/
#define DIRCTION (FORWARD+RISING)


extern int16_t Angle,Angle_120,Angle_240;
/*****POSIF*****/

extern uint8_t Halps_F[];
extern uint8_t Halps_B[];
extern uint8_t HALL_NOW_IN;
extern uint8_t HALL_count;
extern uint16_t Mcsms[];


/*****STATE*****/
typedef enum
{
	Motor_Stop,
	Motor_Lock,
	Motor_Start,
	Motor_Run

}Motor_State;
extern Motor_State Motor_flag;


/*****START*****/
#define Uq_Lock 1500
#define Uq_step 100
#define Uq_Start 2500
#define Lock_time 16000
#define Step_time 500
extern uint16_t Lock_count;
extern uint16_t Openloop_count;
extern uint16_t Openloop_step;
extern uint16_t Start_count;

extern uint16_t z;
/*****SPEED*****/
#define SPEED_MAX (5600)
extern uint16_t Speed_buff;
extern uint16_t Speed;
extern uint32_t speed_time[12];
extern uint64_t speed_time_sum;
extern volatile uint8_t speed_count;
extern volatile uint8_t speed_closeloop;
extern volatile uint8_t Speedtime_count;
extern uint8_t speed_get_flag;


/*****CURRENT*****/
extern int32_t Iq0;
extern int32_t Id0;
extern int16_t Iq;
extern int16_t Iq_Ref;
extern int16_t Iq_Ref0;
extern int16_t Id;
extern int16_t Id_Ref;
extern uint16_t IU_value;
extern uint16_t IV_value;
extern int16_t IUAC_value;
extern int16_t IVAC_value;
extern int32_t IUAC_mA;
extern int32_t IVAC_mA;
extern int32_t IWAC_mA;

extern int32_t IUAC_value_in[8];
extern int32_t IVAC_value_in[8];
extern int32_t IWAC_value_in[8];
extern uint8_t IVAC_count;
extern uint8_t IUAC_count;

/*****ADC*****/
extern uint8_t adc_flag;
extern uint16_t R_value;
extern uint16_t Sin_value;
extern uint16_t Cos_value;
extern uint16_t Angle_value;
extern uint16_t r_value_in[8];
extern uint16_t sin_value_in[8];
extern uint16_t cos_value_in[8];
extern uint16_t angle_value_in[11];
extern uint16_t angle_value;
extern uint16_t angle_value_last;
extern uint16_t angle_gap[10];
extern uint16_t adc_count;
extern uint8_t angle_count;
extern uint16_t sin_adc_count;
extern uint16_t cos_adc_count;
extern uint32_t sincos_count;
extern int16_t IU_offset;
extern int16_t IV_offset;

extern uint16_t sin_sum;
extern uint16_t cos_sum;
extern uint16_t r_sum;

extern uint16_t sin_max;
extern uint16_t sin_min;
extern uint16_t cos_max;
extern uint16_t cos_min;
extern uint16_t sin_max_temp;
extern uint16_t sin_min_temp;
extern uint16_t cos_max_temp;
extern uint16_t cos_min_temp;
extern uint16_t sin_max_temp_last;
extern uint16_t cos_max_temp_last;
extern uint16_t sin_min_temp_last;
extern uint16_t cos_min_temp_last;
extern uint8_t  sincos_flag;
extern uint16_t IU_max;
extern uint16_t IU_min;
extern uint16_t IV_max;
extern uint16_t IV_min;
extern uint16_t sin_dc_component;
extern uint16_t cos_dc_component;
extern uint16_t IU_dc_component;
extern uint16_t IV_dc_component;
extern int16_t  Sin_Normalization;
extern int16_t  Cos_Normalization;
extern int16_t  Angle_temple;
extern int16_t Angle_feedback;
extern int16_t Cosangle_feedback; 
extern uint16_t Angle_rotor;
extern uint8_t sin_threshold_flag;
extern uint8_t cos_threshold_flag;
extern uint8_t IU_threshold_flag;
extern uint8_t IV_threshold_flag;

extern int16_t Angle_feedback1;
extern int16_t Angle_feedback2;

/*****UART*****/
extern uint8_t Oscilloscope_flag;
extern uint32_t Oscilloscope_Data[4];
extern uint8_t Uart_count;
extern uint32_t Buff_head_last;
extern uint32_t Buff_lenth_once;
extern uint8_t Receive_data[128];
extern uint8_t start_position;
extern uint8_t mid_position;
extern uint8_t end_position;
extern uint8_t cmdstart_position;
extern uint8_t cmdend_position;


extern uint16_t var[8]; 

/*****PID*****/
extern uint16_t Speed_Ref;
extern uint16_t Kp;
extern uint16_t Ki;
extern uint16_t Kd;
extern uint16_t AKp;
extern uint16_t AKi;
extern volatile uint16_t PID_count;
extern volatile uint16_t APID_count;
extern int32_t PID_output;
extern int16_t AIvalue_limit;
extern int16_t PID_Limit;
extern uint16_t PID_Period;
extern int16_t output_limit;
extern int16_t s_error1;
extern int16_t s_error2;
extern int16_t s_error3;
extern int32_t P_value;
extern int32_t I_value;
extern int32_t D_value;
extern uint16_t c1;
extern uint16_t c2;
extern uint16_t c3;

extern int32_t APID_output;
extern int16_t APID_Limit;
extern int16_t Aoutput_limit;
extern int16_t a_error1;
extern int16_t a_error2;
extern int32_t AP_value;
extern int32_t AI_value;

extern uint32_t a_error;


extern volatile uint16_t CPID_count;
extern int32_t  Iq_PID_output;
extern int16_t  Iq_PID_Limit;
extern int32_t  Id_PID_output;
extern int16_t  Id_PID_Limit;
extern int16_t Iq_Error1;
extern int16_t Iq_Error2;
extern int32_t Iq_Pvalue;
extern int32_t Iq_Ivalue;
extern uint16_t Iq_Kp;
extern uint16_t Iq_Ki;
extern uint16_t Iq_Kp_temp;
extern uint16_t Iq_Ki_temp;

extern int16_t Id_Error1;
extern int16_t Id_Error2;
extern int32_t Id_Pvalue;
extern int32_t Id_Ivalue;
extern uint16_t Id_Kp;
extern uint16_t Id_Ki;


/*****HALLFOC*****/
extern uint16_t theta_step;
extern volatile uint16_t step_count;
extern uint16_t theta_offset;
extern uint16_t theta_count;
extern volatile uint16_t theta_gap;


/*****clarke-park*****/
extern uint16_t theta;
extern int16_t theta_test;
extern uint16_t theta_start;
extern int16_t Ud;
extern int16_t Uq;
extern int32_t Ua0;
extern int32_t Ub0;
extern int16_t Ua;
extern int16_t Ub;


void Parameter_Init(void);
void PID_Init(void);

#endif
