#include "Interrupt.h"
#include "Parameter.h"
#include "UART.h"
#include "DAC.h"
#include "SVPWM.h"
#include "FOC.h"
#include "POSIF.h"
#include "HALL_Function.h"
#include "VCAN_computer.h"
#include "UART.h"
#include <xmc_uart.h>
#include <xmc_gpio.h>
#include "serial.h"

void SysTick_Handler(void)
{
//		speed_closeloop++;
//	  if(Motor_flag==Motor_Run)
//	  {
//				if(speed_closeloop>=10)
//				{
//					 Speed_closeloop();
//					 speed_closeloop = 0;
//				}
//		}
}

void CCU40_0_IRQHandler(void)
{
	if((RD_REG(CCU40_CC40->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
		SET_BIT(CCU40_CC40->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}

void CCU40_1_IRQHandler(void)
{
	if((RD_REG(CCU40_CC41->INTS, CCU4_CC4_INTS_E0AS_Msk, CCU4_CC4_INTS_E0AS_Pos))==1)
	{
//		if(Motor_flag==Motor_Start||Motor_flag==Motor_Run)
//		{
//				Speed_Calculate();
//		}
		SET_BIT(CCU40_CC41->SWR, CCU4_CC4_SWR_RE0A_Pos); 
	}
}

void CCU40_2_IRQHandler(void)
{
	if((RD_REG(CCU40_CC42->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
		SET_BIT(CCU40_CC42->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}

void CCU40_3_IRQHandler(void)
{
	if((RD_REG(CCU40_CC43->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
		SET_BIT(CCU40_CC43->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}


void CCU80_0_IRQHandler(void)
{
	if((RD_REG(CCU80_CC80->INTS, CCU8_CC8_INTS_OMDS_Msk, CCU8_CC8_INTS_OMDS_Pos))==1)
	{
		switch(Motor_flag)
		{
			case Motor_Stop:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC81->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC82->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80->GCSS |=0x00000111;	
				break;
			}
			case Motor_Lock:
			{
				Park_inverse(0);
				SVPWM16_7(Ua,Ub);
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;			
				CCU80->GCSS |=0x00000111;
				break;
			}
			case Motor_Start:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
				CCU80->GCSS |=0x00000111;
				break;
			}
			case Motor_Run:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
				CCU80->GCSS |=0x00000111;
				break;
			}
			default :
				break;
		}
		SET_BIT(CCU80_CC80->SWR, CCU8_CC8_SWR_ROM_Pos);		
	}
}
void USIC0_0_IRQHandler(void)
{
  static uint8_t data;
	static int i=0;
  data = XMC_UART_CH_GetReceivedData(SERIAL_UART);
  ring_buffer_put(&serial_buffer, data);
	for(;i<serial_buffer.head;i++)
	{
		Receive_data[i] = *(serial_buffer.buffer+i);
		if(*(serial_buffer.buffer+i)=='d')
		{
			 cmdstart_position = i;
		}
		if(*(serial_buffer.buffer+i)=='g')
		{
			 cmdend_position = i;
		}
		if(*(serial_buffer.buffer+i)=='s')
		{
			 start_position = i;
		}
		if(*(serial_buffer.buffer+i)==' ')
		{
			 mid_position = i;
		}
		if(*(serial_buffer.buffer+i)=='e')
		{
			 end_position = i;
		}
	}
}

void VADC0_G1_0_IRQHandler(void)
{
		if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV0_Msk, VADC_G_REFLAG_REV0_Pos))==1)
		{
			if(sincos_flag==0&&(Motor_flag==Motor_Start||Motor_flag==Motor_Run))
			{
				sincos_count++;
			}
			if(sincos_count>=200000)
			{
				sincos_flag=1;
				sincos_count=0;
			}
			Sin_value=(VADC_G1->RESD[0])&0x0000FFFF;
			Cos_value=(VADC_G0->RESD[0])&0x0000FFFF;
			if(Sin_value > sin_max_temp)	sin_max_temp = Sin_value;
			if(Sin_value < sin_min_temp)	sin_min_temp = Sin_value;
			if(Cos_value > cos_max_temp)	cos_max_temp = Cos_value;
			if(Cos_value < cos_min_temp)	cos_min_temp = Cos_value;
			if(sincos_flag==0)
			{
				sin_max = cos_max = 3150;
				sin_min = cos_min = 1750;
			}
			else
			{
				sin_max = sin_max_temp;
				cos_max = cos_max_temp;
				sin_min = sin_min_temp;
				cos_min = cos_min_temp;
			}
			sin_dc_component=(sin_max+sin_min)>>1;
			cos_dc_component=(cos_max+cos_min)>>1;
			Theta_Calculate();                 //角度计算时间越长，电流越正弦
			SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV0_Pos); 
		}
		if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV6_Msk, VADC_G_REFLAG_REV6_Pos))==1)
		{
			int j=0,Iq_Kp_temp = 0,Iq_Ki_temp = 0,Speed_buff_temp=0;
			for(j=cmdstart_position+1;j<cmdend_position;j++)
			{
				Speed_buff_temp = Speed_buff_temp*10 + Receive_data[j] - 48;
			}
			Speed_buff = Speed_buff_temp;
			for(j=start_position+1;j<mid_position;j++)
			{
				Iq_Kp_temp = Iq_Kp_temp*10 + Receive_data[j] - 48;
			}
			Iq_Kp = Iq_Kp_temp;
			for(j=mid_position+1;j<end_position;j++)
			{
				Iq_Ki_temp = Iq_Ki_temp*10 + Receive_data[j] - 48;
			}
			Iq_Ki = Iq_Ki_temp;
			IUAC_mA=(((VADC_G1->RESD[6])&0x0000FFFF)-IU_dc_component)*92;                         //原来是乘以128的，现在转换为mA单位，乘以92
			IVAC_mA=(((VADC_G0->RESD[6])&0x0000FFFF)-IV_dc_component)*92;
			IWAC_mA=-IUAC_mA-IVAC_mA;
			if(Motor_flag==Motor_Start||Motor_flag==Motor_Run)
			{
				Current_closeloop();
				Park_inverse(Angle_rotor);
				SVPWM16_7(Ua,Ub);
			}
			SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV6_Pos); 
		}
}
