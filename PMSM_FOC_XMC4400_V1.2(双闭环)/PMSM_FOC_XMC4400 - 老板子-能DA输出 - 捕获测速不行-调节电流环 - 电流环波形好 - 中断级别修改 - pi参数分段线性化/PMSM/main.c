#include <XMC4400.h>
#include "POSIF.h"
#include "UART.h"
#include "ADC.h"
#include "CCU4.h"
#include "CCU8.h"
#include "GPIO.h"
#include "FOC.h"
#include "Parameter.h"
#include "HALL_Function.h"
#include "DAC.h"
#include "serial.h"
#include "shell.h"

int main(void)
{
  	Parameter_Init();
 	  SysTick_Config(SystemCoreClock/1000);
		GPIO_Init();
		serial_init();
		ADC_Init();
		POSIF0_Init();
		CCU40_Init();	
		CCU80_Init();
		DAC_Init();
		CCU80_Start();
		//CCU40_CC43_Start();
		//CCU40_CC42_Start();
	  //CCU40_CC41_Start();
	  //CCU40_CC40_Start();
		while(1)
		{
			if(Speed_buff==9)
			{
				 Parameter_Init();
				 PID_Init();
			}
			if(Motor_flag==Motor_Stop&&Speed_buff==1)
			{
				 Motor_flag=Motor_Start;                   //Uq=5000时，Iq大约为8000多,Uq=10000时，Iq大约为16000多
				 //Speed_Ref = 300;
				 Iq_Ref = 10000;
				 Id_Ref = 0;
//				 Uq =-3000;
//				 Ud =0;
			}
			if(Motor_flag==Motor_Start&&Speed_buff==2)
			{
				 Motor_flag=Motor_Run;
				 //Speed_Ref = 600;
				 Iq_Ref = 15000;
				 Id_Ref =0;
			}
			if(Motor_flag==Motor_Run)
			{
				if(Speed_buff==3)
				{
					 Iq_Ref =20000;
					 Id_Ref =0;
				}
				if(Speed_buff==4)
				{
					 //Speed_Ref = 2000;                      //电流环参数不变，转速到2000的时候电流正弦度还可以
					 Iq_Ref = 21000;
					 Id_Ref =0;
				}
				if(Speed_buff==5)
				{
					 Iq_Ref = 22000;
					 Id_Ref =0;
				}
				if(Speed_buff==6)
				{
					 //Speed_Ref = 3000;                      //电流环参数不变，转速到2000的时候电流正弦度还可以
					 Iq_Ref = 23000;
					 Id_Ref =0;
				}
				if(Speed_buff==7)
				{
					 //Speed_Ref = 3500;
					 Iq_Ref = 24000;
					 Id_Ref =0;
				}
				if(Speed_buff==8)
				{
					 //Speed_Ref = 4000;                      //电流环参数不变，转速到2000的时候电流正弦度还可以
					 Iq_Ref = 25000;
					 Id_Ref =0;
				}
				if(Speed_buff==9)
				{
					 //Speed_Ref = 4500;
				}
			}
		}
}
