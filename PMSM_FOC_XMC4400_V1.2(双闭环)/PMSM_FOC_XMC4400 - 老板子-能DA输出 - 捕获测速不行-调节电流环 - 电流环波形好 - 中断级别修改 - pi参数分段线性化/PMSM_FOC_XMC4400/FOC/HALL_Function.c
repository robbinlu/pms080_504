#include "HALL_Function.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "FOC.h"

uint16_t Theta_Calculate(void)
{
	  sin_dc_component=(sin_max+sin_min)>>1;
	  cos_dc_component=(cos_max+cos_min)>>1;
    Sin_Normalization=((Sin_value-sin_dc_component)<<14)/(sin_max-sin_dc_component);
	  Cos_Normalization=((Cos_value-cos_dc_component)<<14)/(cos_max-cos_dc_component);
	  //a_error=(Sin_Normalization*Sin_Normalization+Cos_Normalization*Cos_Normalization)/10;
		Angle_PID();
    return 0;
}

void Start_Moter(void)
{	
	HALL_NOW_IN=((POSIF0->PDBG)&0x00e0)>>5;
	Angle_correct(HALL_NOW_IN);	
}	


void Speed_calculate(void)
{
   
}

void Speed_Calculate(void)
{
	uint32_t speed_temp;
	if(speed_count>11)
	{
		speed_count=0;
		speed_get_flag=1;
	}
	if(speed_get_flag==0)
	{
		speed_time[speed_count]=((CCU40_CC42->CV[0]&0x0000FFFF)<<16)+(CCU40_CC41->CV[0]&0x0000FFFF);		
		speed_time_sum+=speed_time[speed_count];
	}
	if(speed_get_flag==1)
	{
		speed_temp=speed_time[speed_count];
		speed_time[speed_count]=((CCU40_CC42->CV[0]&0x0000FFFF)<<16)+(CCU40_CC41->CV[0]&0x0000FFFF);
		speed_time_sum=speed_time_sum+speed_time[speed_count]-speed_temp;
		Speed=60000000/speed_time_sum;
	}
	speed_count++;
}


void Angle_Adjust(void)
{
	if(step_count>0)
	{
		if(theta<theta_step)
		{
			theta=theta+3600-theta_step;
		}
		else
		{
			theta=theta-theta_step;
		}
		if(theta_gap<theta_count&&theta_gap>0)
		{
			if(theta<1)
			{
				theta=theta+3600-1;
			}
			else
			{
				theta--;;
			}
			theta_gap--;
		}
		step_count--;
	}	
}


void Angle_get(uint8_t hall)
{
#if RISING
	#if FORWARD
		switch(hall)
		{
			case 1:
			{
				theta=3300+theta_offset;//150+180
				break;
			}
			case 2:
			{
				theta=900+theta_offset;//270+180
				break;
			}
			case 3:
			{
				theta=300+theta_offset;//210+180
				break;
			}
			case 4:
			{
				theta=2100+theta_offset;//30+180
				break;
			}
			case 5:
			{
				theta=2700+theta_offset;//90+180
				break;
			}
			case 6:
			{
				theta=1500+theta_offset;//330+180
				break;
			}
			default:
				break;	
		}
	#else 
		switch(hall)
		{
			case 1:
			{
				theta=2100+theta_offset;
				break;
			}
			case 2:
			{
				theta=3300+theta_offset;
				break;
			}
			case 3:
			{
				theta=2700+theta_offset;
				break;
			}
			case 4:
			{
				theta=900+theta_offset;
				break;
			}
			case 5:
			{
				theta=1500+theta_offset;
				break;
			}
			case 6:
			{
				theta=300+theta_offset;
				break;
			}
			default:
				break;	
		}	
	#endif
#else
	#if FORWARD
		switch(hall)
		{
			case 1:
			{
				theta=300+theta_offset;
				break;
			}
			case 2:
			{
				theta=1500+theta_offset;
				break;
			}
			case 3:
			{
				theta=900+theta_offset;
				break;
			}
			case 4:
			{
				theta=2700+theta_offset;
				break;
			}
			case 5:
			{
				theta=3300+theta_offset;
				break;
			}
			case 6:
			{
				theta=2100+theta_offset;
				break;
			}
			default:
				break;	
		}	
	#else 
		switch(hall)
		{
			case 1:
			{
				theta=1500+theta_offset;//330+180
				break;
			}
			case 2:
			{
				theta=2700+theta_offset;//90+180
				break;
			}
			case 3:
			{
				theta=210+theta_offset;//30+180
				break;
			}
			case 4:
			{
				theta=300+theta_offset;//210+180
				break;
			}
			case 5:
			{
				theta=900+theta_offset;//270+180
				break;
			}
			case 6:
			{
				theta=3300+theta_offset;//150+180
				break;
			}
			default:
				break;	
		}		
	#endif
#endif		
}


void Angle_correct(uint8_t hall)
{
#if RISING
	#if FORWARD
		switch(hall)
		{
			case 1:
			{
				POSIF0->HALPS	=	Halps_F[0];
				theta=3300+theta_offset;//150+180
				break;
			}
			case 2:
			{
				POSIF0->HALPS	=	Halps_F[1];	
				theta=900+theta_offset;//270+180
				break;
			}
			case 3:
			{
				POSIF0->HALPS	=	Halps_F[2];	
				theta=300+theta_offset;//210+180
				break;
			}
			case 4:
			{
				POSIF0->HALPS	=	Halps_F[3];	
				theta=2100+theta_offset;//30+180
				break;
			}
			case 5:
			{
				POSIF0->HALPS	=	Halps_F[4];	
				theta=2700+theta_offset;//90+180
				break;
			}
			case 6:
			{
				POSIF0->HALPS	=	Halps_F[5];	
				theta=1500+theta_offset;//330+180
				break;
			}
			default:
				break;	
		}
	#else 
		switch(hall)
		{
			case 1:
			{
				POSIF0->HALPS	=	Halps_B[0];
				theta=2100+theta_offset;
				break;
			}
			case 2:
			{
				POSIF0->HALPS	=	Halps_B[1];	
				theta=3300+theta_offset;
				break;
			}
			case 3:
			{
				POSIF0->HALPS	=	Halps_B[2];	
				theta=2700+theta_offset;
				break;
			}
			case 4:
			{
				POSIF0->HALPS	=	Halps_B[3];	
				theta=900+theta_offset;
				break;
			}
			case 5:
			{
				POSIF0->HALPS	=	Halps_B[4];	
				theta=1500+theta_offset;
				break;
			}
			case 6:
			{
				POSIF0->HALPS	=	Halps_B[5];	
				theta=300+theta_offset;
				break;
			}
			default:
				break;	
		}	
	#endif
#else
	#if FORWARD
		switch(hall)
		{
			case 1:
			{
				POSIF0->HALPS	=	Halps_B[0];
				theta=300+theta_offset;
				break;
			}
			case 2:
			{
				POSIF0->HALPS	=	Halps_B[1];	
				theta=1500+theta_offset;
				break;
			}
			case 3:
			{
				POSIF0->HALPS	=	Halps_B[2];	
				theta=900+theta_offset;
				break;
			}
			case 4:
			{
				POSIF0->HALPS	=	Halps_B[3];	
				theta=2700+theta_offset;
				break;
			}
			case 5:
			{
				POSIF0->HALPS	=	Halps_B[4];	
				theta=3300+theta_offset;
				break;
			}
			case 6:
			{
				POSIF0->HALPS	=	Halps_B[5];	
				theta=2100+theta_offset;
				break;
			}
			default:
				break;	
		}	
	#else 
		switch(hall)
		{
			case 1:
			{
				POSIF0->HALPS	=	Halps_F[0];
				theta=1500+theta_offset;//330+180
				break;
			}
			case 2:
			{
				POSIF0->HALPS	=	Halps_F[1];	
				theta=2700+theta_offset;//90+180
				break;
			}
			case 3:
			{
				POSIF0->HALPS	=	Halps_F[2];	
				theta=2100+theta_offset;//30+180
				break;
			}
			case 4:
			{
				POSIF0->HALPS	=	Halps_F[3];	
				theta=300+theta_offset;//210+180
				break;
			}
			case 5:
			{
				POSIF0->HALPS	=	Halps_F[4];	
				theta=900+theta_offset;//270+180
				break;
			}
			case 6:
			{
				POSIF0->HALPS	=	Halps_F[5];	
				theta=3300+theta_offset;//150+180
				break;
			}
			default:
				break;	
		}		
	#endif
#endif	
	
	WR_REG(POSIF0->MCMS, POSIF_MCMS_STHR_Msk, POSIF_MCMS_STHR_Pos, 1);
}





/*
void HPWM_LON(uint8_t hall)
{
#if (RISING==1)
	#if (FORWARD==1)
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_F[0];
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_F[1];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_F[2];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_F[3];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_F[4];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;		
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_F[5];	
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#else
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_B[0];
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_B[1];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;		
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_B[2];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_B[3];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_B[4];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_B[5];	
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;	
			break;
		}
		default:
			break;	
	}
	#endif
#else
	#if (FORWARD==0)
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_B[0];
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_B[1];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_B[2];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_B[3];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_B[4];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;		
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_B[5];	
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#else
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_F[0];
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_F[1];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;		
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_F[2];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_F[3];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_F[4];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_F[5];	
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;	
			break;
		}
		default:
			break;	
	}
	#endif	
#endif	
	
	
	WR_REG(POSIF0->MCMS, POSIF_MCMS_STMR_Msk, POSIF_MCMS_STMR_Pos, 1);				
	CCU80_CC80->CR1S	=	PDC_U;
	CCU80_CC81->CR1S	=	PDC_V;
	CCU80_CC82->CR1S	=	PDC_W;
	WR_REG(POSIF0->MCMS, POSIF_MCMS_STHR_Msk, POSIF_MCMS_STHR_Pos, 1);
}


void HPWM_LPWM(uint8_t hall)
{
#if (RISING==1)
	#if (FORWARD==1)
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_F[0];
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_F[1];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_F[2];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_F[3];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_F[4];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;		
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_F[5];	
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#else
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_B[0];
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_B[1];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;		
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_B[2];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_B[3];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_B[4];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_B[5];	
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#endif
#else
	#if (FORWARD==0)
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_B[0];
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_B[1];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_B[2];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_B[3];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_B[4];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;		
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_B[5];	
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#else
	switch(hall)
	{
		case 1:
		{
			POSIF0->HALPS	=	Halps_F[0];
			POSIF0->MCSM	=	Mcsms[3];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=cmp_Val_CCU8;
			break;
		}
		case 2:
		{
			POSIF0->HALPS	=	Halps_F[1];	
			POSIF0->MCSM	=	Mcsms[5];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;		
			break;
		}
		case 3:
		{
			POSIF0->HALPS	=	Halps_F[2];	
			POSIF0->MCSM	=	Mcsms[4];
			PDC_U=cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 4:
		{
			POSIF0->HALPS	=	Halps_F[3];	
			POSIF0->MCSM	=	Mcsms[1];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period+1;
			break;
		}
		case 5:
		{
			POSIF0->HALPS	=	Halps_F[4];	
			POSIF0->MCSM	=	Mcsms[2];
			PDC_U=SVPWM16.PWM_Period-cmp_Val_CCU8;
			PDC_V=SVPWM16.PWM_Period+1;
			PDC_W=cmp_Val_CCU8;	
			break;
		}
		case 6:
		{
			POSIF0->HALPS	=	Halps_F[5];	
			POSIF0->MCSM	=	Mcsms[0];
			PDC_U=SVPWM16.PWM_Period+1;
			PDC_V=cmp_Val_CCU8;
			PDC_W=SVPWM16.PWM_Period-cmp_Val_CCU8;	
			break;
		}
		default:
			break;	
	}
	#endif	
#endif	
	
	
	WR_REG(POSIF0->MCMS, POSIF_MCMS_STMR_Msk, POSIF_MCMS_STMR_Pos, 1);				
	CCU80_CC80->CR1S	=	PDC_U;
	CCU80_CC81->CR1S	=	PDC_V;
	CCU80_CC82->CR1S	=	PDC_W;
	WR_REG(POSIF0->MCMS, POSIF_MCMS_STHR_Msk, POSIF_MCMS_STHR_Pos, 1);
}
*/

