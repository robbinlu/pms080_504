#include "ADC.h"

//sin,cos,ab相电流，电位器，两个温敏电阻
void ADC_Init(void)
{
	WR_REG(SCU_RESET->PRSET0, SCU_RESET_PRSET0_VADCRS_Msk, SCU_RESET_PRSET0_VADCRS_Pos, 1);
	WR_REG(SCU_RESET->PRCLR0, SCU_RESET_PRCLR0_VADCRS_Msk, SCU_RESET_PRCLR0_VADCRS_Pos, 1);	
	
	WR_REG(VADC->CLC, VADC_CLC_DISR_Msk, VADC_CLC_DISR_Pos, 0);                             //开请求：启用模块时钟
//	while((RD_REG(VADC->CLC, VADC_CLC_DISS_Msk, VADC_CLC_DISS_Pos))!= 0);                   //等待模块启用 
//	WR_REG(VADC->GLOBCFG, VADC_GLOBCFG_SUCAL_Msk, VADC_GLOBCFG_SUCAL_Pos, 1);               //启动初始校准阶段

//	VADC->GLOBCFG = 
//			((0x1 << VADC_GLOBCFG_DIVWC_Pos) & VADC_GLOBCFG_DIVWC_Msk)                          //对分配参数的写控制，DIVA，DCMSB，DIVD位段可被写入
//			| ((0x0 << VADC_GLOBCFG_DIVA_Pos) & VADC_GLOBCFG_DIVA_Msk);                         //fadc1=fadc/2
//	
//	
//	VADC_G1->QCTRL0 =
//			((0x1 << VADC_G_QCTRL0_XTWC_Pos) & VADC_G_QCTRL0_XTWC_Msk) |                        //触发配置的写控制，可以写入 XTMODE 和 XTSEL 位段
//			((0x8 << VADC_G_QCTRL0_XTSEL_Pos) & VADC_G_QCTRL0_XTSEL_Msk)                        //
//			| ((0x2 << VADC_G_QCTRL0_XTMODE_Pos) & VADC_G_QCTRL0_XTMODE_Msk) ;	                //在上升沿上产生触发事件	
//	
//	WR_REG(VADC_G1->QMR0, VADC_G_QMR0_ENTR_Msk, VADC_G_QMR0_ENTR_Pos, 1);	                  //如果在队列 0 寄存器或在后台寄存器中有被挂起的有效转换请求， 发出转换请求
//	
//	VADC_G0->QINR0 =
//			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                              //自动重新填充：当相关的变换被启动， 该队列自动重新加载到 QINRx
//			((0x0 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                      //被转换为通道4
//			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk) ;	                      //在发出一个变换请求之前，有效队列条目等待一个触发事件产生。
//	
//	VADC_G1->QINR0 =
//			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                              //自动重新填充：当相关的变换被启动， 该队列自动重新加载到 QINRx                             
//			((0x1 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                      //被转换为通道4
//			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk);                         //在发出一个变换请求之前，有效队列条目等待一个触发事件产生。
//			
//			
//	VADC_G1->QINR0 =
//			((0x01 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                             //自动重新填充：当相关的变换被启动， 该队列自动重新加载到 QINRx
//			((0x02 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk);	                  //被转换为通道2
//	WR_REG(VADC_G1->CHCTR[2], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 2);         //保存结果到组结果寄存器 GxRES2
//	WR_REG(VADC_G1->RCR[2], VADC_G_RCR_DMM_Msk, VADC_G_RCR_DMM_Pos, 1);	                    //结果过滤模式	
//	WR_REG(VADC_G1->RCR[2], VADC_G_RCR_DRCTR_Msk, VADC_G_RCR_DRCTR_Pos, 0x0F);		          //???	
//	WR_REG(VADC_G1->RCR[2], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                 //在一个结果事件之后发出服务请求
//	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV2NP_Msk, VADC_G_REVNP0_REV2NP_Pos, 0);         //连接相应的事件触发信号到一个服务请求线0,选择组1的服务请求线路 3
//						

//	WR_REG(VADC_G0->CHASS, VADC_G_CHASS_ASSCH3_Msk, VADC_G_CHASS_ASSCH3_Pos, 1);            //通道3是在组0内的一个优先通道      		
//	WR_REG(VADC_G1->CHASS, VADC_G_CHASS_ASSCH3_Msk, VADC_G_CHASS_ASSCH3_Pos, 1);            //通道3是在组1内的一个优先通道 	

//	WR_REG(VADC_G0->CHCTR[4], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 4);	        //保存结果到组结果寄存器G0RES4
//	WR_REG(VADC_G1->CHCTR[4], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 4);	        //保存结果到组结果寄存器G1RES4
//	
//	WR_REG(VADC_G0->SYNCTR, VADC_G_SYNCTR_STSEL_Msk, VADC_G_SYNCTR_STSEL_Pos, 1);           //内核是同步从内核：控制信息来自输入 C11
//	WR_REG(VADC_G0->SYNCTR, VADC_G_SYNCTR_EVALR1_Msk, VADC_G_SYNCTR_EVALR1_Pos, 1);         //就绪输入 Rx 被认为是为该转换组的一次并行转换的开始
//	WR_REG(VADC_G1->CHCTR[4], VADC_G_CHCTR_SYNC_Msk, VADC_G_CHCTR_SYNC_Pos, 1);             //请求对该通道进行一次同步转换（只考虑一个主内核）
//	WR_REG(VADC_G1->ARBCFG, VADC_G_ARBCFG_ANONC_Msk, VADC_G_ARBCFG_ANONC_Pos, 3);           //正常工作模式
//	WR_REG(VADC_G0->QMR0, VADC_G_QMR0_ENGT_Msk, VADC_G_QMR0_ENGT_Pos, 1);                   //如果在队列 0 寄存器或在后台寄存器中有被挂起的有效转换请求，发出转换请求
//	WR_REG(VADC_G1->QMR0, VADC_G_QMR0_ENGT_Msk, VADC_G_QMR0_ENGT_Pos, 1);                   //如果在队列 0 寄存器或在后台寄存器中有被挂起的有效转换请求，发出转换请求
//	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_PRIO0_Msk, VADC_G_ARBPR_PRIO0_Pos, 3);              //请求的源0的仲裁优先级（在时隙0内）选择最高优先级              
//	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_PRIO0_Msk, VADC_G_ARBPR_PRIO0_Pos, 3);              //请求的源0的仲裁优先级（在时隙0内）选择最高优先级 
//	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_CSM0_Msk, VADC_G_ARBPR_CSM0_Pos, 1);                //取消-插入-重复模式，即该源可以取消其它源的转换。
//	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_CSM0_Msk, VADC_G_ARBPR_CSM0_Pos, 1);                //取消-插入-重复模式，即该源可以取消其它源的转换。
//	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_ASEN0_Msk, VADC_G_ARBPR_ASEN0_Pos, 1);              //启用相应的仲裁时隙。仲裁器对来自关联请求源的挂起转换请求被仲裁。
//	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_ASEN0_Msk, VADC_G_ARBPR_ASEN0_Pos, 1);              //启用相应的仲裁时隙。仲裁器对来自关联请求源的挂起转换请求被仲裁。
//	while((RD_REG(VADC_G0->ARBCFG, VADC_G_ARBCFG_CAL_Msk, VADC_G_ARBCFG_CAL_Pos))!= 0);     //初始校准完成
//	while((RD_REG(VADC_G1->ARBCFG, VADC_G_ARBCFG_CAL_Msk, VADC_G_ARBCFG_CAL_Pos))!= 0);	    //初始校准完成


//	WR_REG(VADC_G0->RCR[4], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                 //在一个结果事件之后发出服务请求
//	WR_REG(VADC_G0->REVNP0, VADC_G_REVNP0_REV0NP_Msk, VADC_G_REVNP0_REV0NP_Msk, 0);         //连接相应的事件触发信号到一个服务请求线，选择组0的服务请求线路 0
//	
//	WR_REG(VADC_G1->RCR[4], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                 //在一个结果事件之后发出服务请求
//	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV0NP_Msk, VADC_G_REVNP0_REV0NP_Msk, 0);         //连接相应的事件触发信号到一个服务请求线，选择组0的服务请求线路 0
//	
//	NVIC_SetPriority(VADC0_G0_0_IRQn, 1);	
//	NVIC_EnableIRQ(VADC0_G0_0_IRQn);
//	
//	NVIC_SetPriority(VADC0_G1_0_IRQn, 1);	
//	NVIC_EnableIRQ(VADC0_G1_0_IRQn);
	while((RD_REG(VADC->CLC, VADC_CLC_DISS_Msk, VADC_CLC_DISS_Pos))!= 0);                    //状态位，模块时钟被使能
	WR_REG(VADC->GLOBCFG, VADC_GLOBCFG_SUCAL_Msk, VADC_GLOBCFG_SUCAL_Pos, 1);                //启动初始校准阶段
	VADC->GLOBCFG =                                                                          
			((0x1 << VADC_GLOBCFG_DIVWC_Pos) & VADC_GLOBCFG_DIVWC_Msk)                           //分频器参数的写控制，写1表示位域DIVA、DCMSB、DIVD可被写入
			| ((0x0 << VADC_GLOBCFG_DIVA_Pos) & VADC_GLOBCFG_DIVA_Msk) ;                         //Fadc1=Fadc
	
	VADC_G1->QCTRL0 =
			((0x1 << VADC_G_QCTRL0_XTWC_Pos) & VADC_G_QCTRL0_XTWC_Msk) |                         //触发配置的写控制，可以写位域XTMODE和XTSEL
			((0x8 << VADC_G_QCTRL0_XTSEL_Pos) & VADC_G_QCTRL0_XTSEL_Msk)                         //GxREQGTH为门控输入？？？
			| ((0x2 << VADC_G_QCTRL0_XTMODE_Pos) & VADC_G_QCTRL0_XTMODE_Msk) ;	                 //在上升沿产生触发事件
	
	WR_REG(VADC_G1->QMR0, VADC_G_QMR0_ENTR_Msk, VADC_G_QMR0_ENTR_Pos, 1);	                   //在所选触发输入信号 REQTR 的所选边沿产生触发事件。
	
	//p14.0  cos
	VADC_G0->QINR0 =
			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                               //自动重填：当相关的转换启动时，该队列条目被自动重新加载到 QINRx。
			((0x0 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                       //定义要被转换的通道号0	
			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk) ;	                       //在发出转换请求之前，有效队列条目等待一个触发事件发生。
			
	//p14.8  sin
	VADC_G1->QINR0 =
			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                               //自动重填：当相关的转换启动时，该队列条目被自动重新加载到 QINRx。
			((0x0 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                       //定义要被转换的通道号0	
			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk);                          //在发出转换请求之前，有效队列条目等待一个触发事件发生。
			
	//P14.9为电位器	
	VADC_G1->QINR0 =
			((0x01 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                              //自动重填：当相关的转换启动时，该队列条目被自动重新加载到 QINRx。
			((0x01 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk);	                   //定义要被转换的通道号7
	WR_REG(VADC_G1->CHCTR[1], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 1);          //保存结果到组1结果寄存器GxRES1        
	WR_REG(VADC_G1->RCR[1], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV1NP_Msk, VADC_G_REVNP0_REV1NP_Pos, 0);          //选择组1的1通道结果事件的服务请求线0
	
  //P14.14  IU
	VADC_G1->QINR0 =
			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                               //自动重填：当相关的转换启动时，该队列条目被自动重新加载到 QINRx。
			((0x6 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                       //定义要被转换的通道号6	
			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk);                          //在发出转换请求之前，有效队列条目等待一个触发事件发生。	

  //P14.6   IV
	VADC_G0->QINR0 =
			((0x1 << VADC_G_QINR0_RF_Pos) & VADC_G_QINR0_RF_Msk) |                               //自动重填：当相关的转换启动时，该队列条目被自动重新加载到 QINRx。
			((0x6 << VADC_G_QINR0_REQCHNR_Pos) & VADC_G_QINR0_REQCHNR_Msk)                       //定义要被转换的通道号0	
			| ((0x1 << VADC_G_QINR0_EXTR_Pos) & VADC_G_QINR0_EXTR_Msk);                          //在发出转换请求之前，有效队列条目等待一个触发事件发生。
			
	WR_REG(VADC_G0->CHASS, VADC_G_CHASS_ASSCH0_Msk, VADC_G_CHASS_ASSCH0_Pos, 1);             //通道0是组0内的一个优先通道		
  WR_REG(VADC_G1->CHASS, VADC_G_CHASS_ASSCH0_Msk, VADC_G_CHASS_ASSCH0_Pos, 1);             //通道0是组1内的一个优先通道
  
  WR_REG(VADC_G0->CHASS, VADC_G_CHASS_ASSCH6_Msk, VADC_G_CHASS_ASSCH6_Pos, 1);             //通道6是组0内的一个优先通道		
  WR_REG(VADC_G1->CHASS, VADC_G_CHASS_ASSCH6_Msk, VADC_G_CHASS_ASSCH6_Pos, 1);             //通道6是组1内的一个优先通道	
  //WR_REG(VADC_G1->CHASS, VADC_G_CHASS_ASSCH1_Msk, VADC_G_CHASS_ASSCH1_Pos, 1);             //通道1是组1内的一个优先通道	

	WR_REG(VADC_G0->CHCTR[0], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 0);          //保存结果到组0结果寄存器GxRES0 	
	WR_REG(VADC_G1->CHCTR[0], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 0);	         //保存结果到组1结果寄存器GxRES0 
	
	WR_REG(VADC_G0->CHCTR[6], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 6);          //保存结果到组0结果寄存器GxRES6 	
	WR_REG(VADC_G1->CHCTR[6], VADC_G_CHCTR_RESREG_Msk, VADC_G_CHCTR_RESREG_Pos, 6);	         //保存结果到组1结果寄存器GxRES6 
	
	WR_REG(VADC_G0->SYNCTR, VADC_G_SYNCTR_STSEL_Msk, VADC_G_SYNCTR_STSEL_Pos, 1);            //该内核是同步从内核：控制信息来自输入 CI1
	WR_REG(VADC_G0->SYNCTR, VADC_G_SYNCTR_EVALR1_Msk, VADC_G_SYNCTR_EVALR1_Pos, 1);          //就绪输入 R1 被看做该转换组的一次并行转换的开始。
	WR_REG(VADC_G1->CHCTR[0], VADC_G_CHCTR_SYNC_Msk, VADC_G_CHCTR_SYNC_Pos, 1);              //请求对该通道进行一次同步转换(仅考虑主内核)
	WR_REG(VADC_G1->CHCTR[6], VADC_G_CHCTR_SYNC_Msk, VADC_G_CHCTR_SYNC_Pos, 1);              //请求对该通道进行一次同步转换(仅考虑主内核)
	WR_REG(VADC_G1->ARBCFG, VADC_G_ARBCFG_ANONC_Msk, VADC_G_ARBCFG_ANONC_Pos, 3);            //模拟转换器为正常工作模式
	WR_REG(VADC_G0->QMR0, VADC_G_QMR0_ENGT_Msk, VADC_G_QMR0_ENGT_Pos, 1);                    //如果在队列 0 寄存器或备份寄存器中有被挂起的有效转换请求，发出转换请求
	WR_REG(VADC_G1->QMR0, VADC_G_QMR0_ENGT_Msk, VADC_G_QMR0_ENGT_Pos, 1);                    //如果在队列 0 寄存器或备份寄存器中有被挂起的有效转换请求，发出转换请求
	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_PRIO0_Msk, VADC_G_ARBPR_PRIO0_Pos, 3);               //请求源0的仲裁优先级为最高
	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_PRIO0_Msk, VADC_G_ARBPR_PRIO0_Pos, 3);               //请求源1的仲裁优先级为最高
	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_CSM0_Msk, VADC_G_ARBPR_CSM0_Pos, 1);                 //请求源0的转换启动模式撤消-插入-重复模式，即该源可以撤消其他源的转换。
	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_CSM0_Msk, VADC_G_ARBPR_CSM0_Pos, 1);                 //请求源1的转换启动模式撤消-插入-重复模式，即该源可以撤消其他源的转换。
	WR_REG(VADC_G0->ARBPR, VADC_G_ARBPR_ASEN0_Msk, VADC_G_ARBPR_ASEN0_Pos, 1);               //相应的仲裁时隙被使能。仲裁器对来自相关联请求源的挂起转换请求进行仲裁。
	WR_REG(VADC_G1->ARBPR, VADC_G_ARBPR_ASEN0_Msk, VADC_G_ARBPR_ASEN0_Pos, 1);               //相应的仲裁时隙被使能。仲裁器对来自相关联请求源的挂起转换请求进行仲裁。
	while((RD_REG(VADC_G0->ARBCFG, VADC_G_ARBCFG_CAL_Msk, VADC_G_ARBCFG_CAL_Pos))!= 0);      //等待初始校准阶段完成
	while((RD_REG(VADC_G1->ARBCFG, VADC_G_ARBCFG_CAL_Msk, VADC_G_ARBCFG_CAL_Pos))!= 0);	     //等待初始校准阶段完成


	WR_REG(VADC_G0->RCR[0], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G0->REVNP0, VADC_G_REVNP0_REV0NP_Msk, VADC_G_REVNP0_REV0NP_Pos, 0);          //选择组0的0通道结果事件的服务请求线0
	
	WR_REG(VADC_G1->RCR[0], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV0NP_Msk, VADC_G_REVNP0_REV0NP_Pos, 0);          //选择组1的0通道结果事件的服务请求线0
	
	WR_REG(VADC_G0->RCR[6], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G0->REVNP0, VADC_G_REVNP0_REV6NP_Msk, VADC_G_REVNP0_REV6NP_Pos, 0);          //选择组0的0通道结果事件的服务请求线0
	
	WR_REG(VADC_G1->RCR[6], VADC_G_RCR_SRGEN_Msk, VADC_G_RCR_SRGEN_Pos, 1);                  //发生结果事件后发出服务请求
	WR_REG(VADC_G1->REVNP0, VADC_G_REVNP0_REV6NP_Msk, VADC_G_REVNP0_REV6NP_Pos, 0);          //选择组1的0通道结果事件的服务请求线0
	NVIC_SetPriority(VADC0_G0_0_IRQn, 2);	
	NVIC_EnableIRQ(VADC0_G0_0_IRQn);
	
	NVIC_SetPriority(VADC0_G1_0_IRQn, 2);	
	NVIC_EnableIRQ(VADC0_G1_0_IRQn);
}

