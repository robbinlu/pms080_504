#include "DAC.h"
#include <xmc_dac.h>


//Channel0-P14.8
//Channel1-P14.9

XMC_DAC_CH_CONFIG_t const ch_config0=
{
  .output_offset	= 0U,
  .data_type 		= XMC_DAC_CH_DATA_TYPE_SIGNED,
  .output_scale 	= XMC_DAC_CH_OUTPUT_SCALE_NONE,
  .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
};

XMC_DAC_CH_CONFIG_t const ch_config1=
{
  .output_offset	= 0U,
  .data_type 		= XMC_DAC_CH_DATA_TYPE_SIGNED,
  .output_scale 	= XMC_DAC_CH_OUTPUT_SCALE_NONE,
  .output_negation = XMC_DAC_CH_OUTPUT_NEGATION_DISABLED,
};



void DAC_Init(void)
{
	//XMC_DAC_CH_Init(XMC_DAC0, 0U, &ch_config0);
  XMC_DAC_CH_Init(XMC_DAC0, 1U, &ch_config1);
	
	//XMC_DAC_CH_StartDataMode(XMC_DAC0, 0U, XMC_DAC_CH_TRIGGER_INTERNAL, 7500000);
	XMC_DAC_CH_StartDataMode(XMC_DAC0, 1U, XMC_DAC_CH_TRIGGER_INTERNAL, 7500000);	
	XMC_DAC_EnableSimultaneousDataMode(XMC_DAC0);
}


void DAC_Output(uint16_t ch0, int16_t ch1)
{
	XMC_DAC_SimultaneousWrite(XMC_DAC0, ch0, ch1);
}
