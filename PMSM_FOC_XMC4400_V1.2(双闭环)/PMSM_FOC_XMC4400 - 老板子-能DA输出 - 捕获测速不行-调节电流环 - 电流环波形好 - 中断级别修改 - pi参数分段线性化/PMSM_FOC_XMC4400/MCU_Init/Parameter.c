#include "Parameter.h"
#include "SVPWM.h"


/*****STATE*****/
Motor_State Motor_flag;


/*****POSIF*****/

uint8_t Halps_F[] ={0x19,0x32,0x13,0x2C,0x0D,0x26};//6-4-5-1-3-2
uint8_t Halps_B[] ={0x29,0x1A,0x0B,0x34,0x25,0x16};//2-3-1-5-4-6
uint8_t HALL_NOW_IN;
uint8_t HALL_count;
uint16_t Mcsms[] = {0x210,0x012,0x102,0x120,0x021,0x201};


/*****START*****/
uint16_t Start_count;
uint16_t Openloop_step;


/*****SPEED*****/
uint16_t Speed;
uint32_t speed_time[12];
uint64_t speed_time_sum;
uint8_t speed_get_flag;
volatile uint8_t speed_count;


/*****CURRENT*****/
int16_t IU_value;
int16_t IV_value;


/*****ADC*****/
uint16_t R_value;
uint16_t Sin_value;
uint16_t Cos_value;
uint16_t Angle_value=0;
uint16_t r_value_in[8];
uint16_t sin_value_in[8];
uint16_t cos_value_in[8];
uint16_t angle_value_in[11];
uint16_t angle_gap[10]={0};
uint16_t adc_count;
uint8_t angle_count;
uint16_t sin_adc_count;
uint16_t cos_adc_count;
int16_t IU_offset;
int16_t IV_offset;

uint16_t sin_sum=0;
uint16_t cos_sum=0;
uint16_t r_sum=0;

uint16_t sin_max=3250;
uint16_t sin_min=1650;
uint16_t cos_max=3250;
uint16_t cos_min=1650;
uint16_t sin_dc_component=2450;
uint16_t cos_dc_component=2450;

int16_t  Sin_Normalization=0;
int16_t  Cos_Normalization=0;
int16_t  Angle_temple=0;
int16_t Angle_feedback=0;
int16_t Cosangle_feedback=0; 
uint16_t Angle_rotor=0;
uint8_t sin_threshold_flag=0;
uint8_t cos_threshold_flag=0;

//test
int16_t Angle_feedback1=0;
int16_t Angle_feedback2=0;


/*****UART*****/
uint8_t Oscilloscope_flag;
uint32_t Oscilloscope_Data[4];
uint8_t Uart_count;

uint8_t var[2]; 

/*****PID*****/
uint16_t Speed_Ref;
uint16_t Kp;
uint16_t Ki;
uint16_t Kd;
uint16_t AKp;
uint16_t AKi;
volatile uint16_t PID_count;
volatile uint16_t APID_count;
int32_t PID_output;
int16_t PID_Limit;
uint16_t PID_Period;
int16_t output_limit;
int16_t s_error1;
int16_t s_error2;
int16_t s_error3;
int32_t P_value;
int32_t I_value;
int32_t D_value;

int32_t APID_output;
int16_t AIvalue_limit;
int16_t APID_Limit;
int16_t Aoutput_limit;
int16_t a_error1;
int16_t a_error2;
int32_t AP_value;
int32_t AI_value;

uint32_t a_error=1000;

/*****HALLFOC*****/
uint16_t theta_step;
uint16_t theta_count;
volatile uint16_t step_count;
uint16_t theta_offset;
uint16_t theta_count;
volatile uint16_t theta_gap;


/*****clarke-park*****/
uint16_t theta;
int16_t theta_test;
uint16_t theta_start;
int16_t Ud;
int16_t Uq;
int32_t Ua0;
int32_t Ub0;
int16_t Ua;
int16_t Ub;



void Parameter_Init(void)
{
	Motor_flag=Motor_Stop;
	SVPWM16.PWM_Period=3750;
	IU_offset=200-2047;
	IV_offset=200-2047;
	Start_count=0;
	speed_count=0;
	adc_count=0;
	angle_count=0;
	sin_adc_count=0;
	cos_adc_count=0;
	speed_get_flag=0;
	Openloop_step=0;
	Uq=0;
	Ud=0;	
	theta=0;
	Oscilloscope_flag=1;	
}

void PID_Init(void)
{
	Kp=350;
	Ki=8000;
	Kd=30;
	PID_count=0;
	PID_Limit=150;
	output_limit=32000;
	s_error1=0;
	s_error2=0;
	s_error3=0;
	
	AKp=45;                //P过大会使得角度的发生振动，输出曲线变粗
	AKi=8;                 //I过大也会使得角度发生振动，输出曲线变粗
	APID_count=0;
	AIvalue_limit=5000;
	APID_Limit=400;
	Aoutput_limit=32000;
	a_error1=0;
	a_error2=0;
}

