#ifndef _PARAMETER_H_
#define _PARAMETER_H_

#include <XMC4400.h>
#define Angle_offset       1095                       //1095      1200
/*****RISING-FALLING*****/
#define RISING (1)

/*****FORWARD-BACKWARD*****/
#define FORWARD (0)

/*****DIRCTION*****/
#define DIRCTION (FORWARD+RISING)


/*****POSIF*****/

extern uint8_t Halps_F[];
extern uint8_t Halps_B[];
extern uint8_t HALL_NOW_IN;
extern uint8_t HALL_count;
extern uint16_t Mcsms[];


/*****STATE*****/
typedef enum
{
	Motor_Stop,
	Motor_Lock,
	Motor_Start,
	Motor_Run

}Motor_State;
extern Motor_State Motor_flag;


/*****START*****/
#define Uq_Lock 4000
#define Uq_step 200
#define Uq_Start 6000
#define Lock_time 2000
#define Step_time 500
extern uint16_t Openloop_step;
extern uint16_t Start_count;


/*****SPEED*****/
#define SPEED_MAX (4200)
extern uint16_t Speed;
extern uint32_t speed_time[12];
extern uint64_t speed_time_sum;
extern volatile uint8_t speed_count;
extern uint8_t speed_get_flag;


/*****CURRENT*****/
extern int16_t IU_value;
extern int16_t IV_value;


/*****ADC*****/
extern uint16_t R_value;
extern uint16_t Sin_value;
extern uint16_t Cos_value;
extern uint16_t Angle_value;
extern uint16_t r_value_in[8];
extern uint16_t sin_value_in[8];
extern uint16_t cos_value_in[8];
extern uint16_t angle_value_in[11];
extern uint16_t angle_gap[10];
extern uint16_t adc_count;
extern uint8_t angle_count;
extern uint16_t sin_adc_count;
extern uint16_t cos_adc_count;
extern int16_t IU_offset;
extern int16_t IV_offset;

extern uint16_t sin_sum;
extern uint16_t cos_sum;
extern uint16_t r_sum;

extern uint16_t sin_max;
extern uint16_t sin_min;
extern uint16_t cos_max;
extern uint16_t cos_min;
extern uint16_t sin_dc_component;
extern uint16_t cos_dc_component;

extern int16_t  Sin_Normalization;
extern int16_t  Cos_Normalization;
extern int16_t  Angle_temple;
extern int16_t Angle_feedback;
extern int16_t Cosangle_feedback; 
extern uint16_t Angle_rotor;
extern uint8_t sin_threshold_flag;
extern uint8_t cos_threshold_flag;

extern int16_t Angle_feedback1;
extern int16_t Angle_feedback2;

/*****UART*****/
extern uint8_t Oscilloscope_flag;
extern uint32_t Oscilloscope_Data[4];
extern uint8_t Uart_count;

extern uint8_t var[2]; 

/*****PID*****/
extern uint16_t Speed_Ref;
extern uint16_t Kp;
extern uint16_t Ki;
extern uint16_t Kd;
extern uint16_t AKp;
extern uint16_t AKi;
extern volatile uint16_t PID_count;
extern volatile uint16_t APID_count;
extern int32_t PID_output;
extern int16_t AIvalue_limit;
extern int16_t PID_Limit;
extern uint16_t PID_Period;
extern int16_t output_limit;
extern int16_t s_error1;
extern int16_t s_error2;
extern int16_t s_error3;
extern int32_t P_value;
extern int32_t I_value;
extern int32_t D_value;

extern int32_t APID_output;
extern int16_t APID_Limit;
extern int16_t Aoutput_limit;
extern int16_t a_error1;
extern int16_t a_error2;
extern int32_t AP_value;
extern int32_t AI_value;

extern uint32_t a_error;


/*****HALLFOC*****/
extern uint16_t theta_step;
extern volatile uint16_t step_count;
extern uint16_t theta_offset;
extern uint16_t theta_count;
extern volatile uint16_t theta_gap;


/*****clarke-park*****/
extern uint16_t theta;
extern int16_t theta_test;
extern uint16_t theta_start;
extern int16_t Ud;
extern int16_t Uq;
extern int32_t Ua0;
extern int32_t Ub0;
extern int16_t Ua;
extern int16_t Ub;


void Parameter_Init(void);
void PID_Init(void);

#endif
