#ifndef UART_H_
#define UART_H_

#include <XMC4400.h>



void UART_Init(void);
void UART_Transmit(uint8_t *dat);
unsigned int CRC_CHECK(unsigned char *Buf, unsigned char CRC_CNT);
void OutPut_Data(uint32_t *speed);


#endif
