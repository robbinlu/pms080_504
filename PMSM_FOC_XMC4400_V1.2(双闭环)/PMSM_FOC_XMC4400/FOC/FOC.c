#include "FOC.h"
#include "Parameter.h"
#include "SVPWM.h"
#include "Table.h"
#include "DAC.h"
#include "Interrupt.h"

void Angle_PID(void)
{
	a_error2=a_error1;
  a_error1=(int16_t)(((Sin_Normalization*sintable16[Cosangle_feedback])-(Cos_Normalization*sintable16[Angle_feedback]))>>14);  
	AP_value=(int32_t)(((a_error1-a_error2)<<6)-((a_error1-a_error2)<<4));
	AI_value=(int32_t)(a_error1<<3);
	APID_output=AP_value+AI_value;
	APID_output=APID_output>>15;
	
	if(APID_output>APID_Limit)
	{
		APID_output=APID_Limit;
	}
	if(APID_output<(0-APID_Limit))
	{
		APID_output=(0-APID_Limit);
	}
	if(((Angle_feedback+APID_output)>=0)&&((Angle_feedback+APID_output)<=3600))
	{
		Angle_feedback=Angle_feedback+APID_output;
	}
	else if((Angle_feedback+APID_output)<0)
	{
		Angle_feedback=Angle_feedback+APID_output+3600;
	}
	else if((Angle_feedback+APID_output)>3600)
	{
		Angle_feedback=Angle_feedback+APID_output-3600;
	}
	if(Angle_feedback>3500&&Angle_feedback<3600)
	{
		sin_max=3250;
		cos_max=3250;
		sin_min=1650;
		cos_min=1650;
	}
	//Angle_feedback=(uint16_t)Angle_temple;
	if(Angle_feedback>2700)                   //cos(a)=sin(a+90��)
	{
		Cosangle_feedback=Angle_feedback-2700;
	}
	else if(Angle_feedback<2700)
	{
		Cosangle_feedback=Angle_feedback+900;
	}
	else if(Angle_feedback==2700)
	{
		Cosangle_feedback=0;
	}
	//DAC_Output(0, y);
	if(Angle_feedback<=Angle_offset) 
	{

		Angle_rotor=((Angle_feedback-Angle_offset+3600)<<2)%3600;
    //Angle_rotor=(4*(Angle_offset-Angle_feedback))%3600;		
	}
	else
	{
	 Angle_rotor=((Angle_feedback-Angle_offset)<<2)%3600;
    //Angle_rotor=(4*(Angle_offset-Angle_feedback+3600))%3600;		
	}
}

void Angle_PID2(void)
{
	 Angle_feedback2=Angle_feedback1;
	 Angle_feedback1=Angle_feedback;
	 a_error2=a_error1;
	 a_error1=(int16_t)((Sin_Normalization*sintable16[Cosangle_feedback]>>13)-(Cos_Normalization*sintable16[Angle_feedback]>>13));
	 AP_value=(int32_t)AKp*a_error1;
	 AI_value=(int32_t)(AKi * a_error2);
	 APID_output=AP_value+AI_value;
	 APID_output=APID_output>>15;
	 
	 if(APID_output>APID_Limit)
	 {
			APID_output=APID_Limit;
	 }
	 if(APID_output<(0-APID_Limit))
	 {
			APID_output=(0-APID_Limit);
	 }
	 Angle_feedback=APID_output+2*Angle_feedback1-Angle_feedback2;
	 if(Angle_feedback>2700)                   //cos(a)=sin(a+90��)
		{
			Cosangle_feedback=Angle_feedback-2700;
		}
		else if(Angle_feedback<2700)
		{
			Cosangle_feedback=Angle_feedback+900;
		}
		else if(Angle_feedback==2700)
		{
			Cosangle_feedback=0;
		}
}


void Park_inverse(uint16_t theta_in)
{
	uint16_t costheta; 
	
	if(theta_in>2700)    //cos(a)=sin(a+90��)
	{
		costheta=theta_in-2700;
	}
	else if(theta_in<2700)
	{
		costheta=theta_in+900;
	}
	else if(theta_in==2700)
	{
		costheta=0;
	}
	Uq=2000;
	Ua0=-Uq*(sintable16[theta_in]);
	Ub0=Uq*(sintable16[costheta]);
	Ua=Ua0>>15;
	Ub=Ub0>>15;
}



