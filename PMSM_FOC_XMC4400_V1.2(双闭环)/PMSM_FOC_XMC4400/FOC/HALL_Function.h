#ifndef _HALL_FUNCTION_H_
#define _HALL_FUNCTION_H_

#include <XMC4400.h>


void Start_Moter(void);
void Angle_get(uint8_t hall);
void Angle_correct(uint8_t hall);
void Angle_Adjust(void);
uint16_t Theta_Calculate(void);
void Speed_Calculate(void);
void Speed_calculate(void);
/*
void HPWM_LON(uint8_t hall);
void HPWM_LPWM(uint8_t hall);
*/

#endif
