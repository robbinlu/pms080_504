#ifndef _DAC_H_
#define _DAC_H_


#include <XMC4400.h>


void DAC_Init(void);
void DAC_Output(uint16_t ch0, int16_t ch1);


#endif
