#include "Interrupt.h"
#include "Parameter.h"
#include "UART.h"
#include "DAC.h"
#include "SVPWM.h"
#include "FOC.h"
#include "POSIF.h"
#include "HALL_Function.h"
#include "VCAN_computer.h"
#include "UART.h"
#include <xmc_uart.h>
int x,y;

void SysTick_Handler(void)
{
	//XMC_UART_CH_Transmit(XMC_UART0_CH1, 0x60);
	//vcan_sendware((uint8_t *)var, sizeof(var));
  OutPut_Data(Oscilloscope_Data);
	
//	if(Motor_flag==Motor_Start)
//	{
//	   Bubble_Sort(sin_value_in);
//	   Bubble_Sort(cos_value_in);
//	}
	/*
	if(Motor_flag==Motor_Lock)
	{
		Uq=Uq_Lock;
		Start_count++;
		if(Start_count>=Lock_time)
		{
			Start_count=0;
			Motor_flag=Motor_Start;
		}
	}
	else if(Motor_flag==Motor_Start)
	{
		if(Start_count>=Step_time)
		{
			Start_count=0;
			Openloop_step++;
			if(Openloop_step<10)
			{
				Uq=Uq+Uq_step;   
				if(Uq>Uq_Start)
				{
					Uq=Uq_Start;
				}
			}
			else
			{
				Motor_flag=Motor_Run;
			}
		}
		else
		{
			Start_count++;			
		}			
	}
*/



/*
	if(Motor_flag==Motor_Run)
	{
		PID_count++;
		if(PID_count>=100)
		{
			Uq=Uq+Speed_PID();
			if(Uq>output_limit)
			{
				Uq=output_limit;
			}
			if(Uq<(0-output_limit))
			{
				Uq=(int16_t)(0-output_limit);
			}
			PID_count=0;
		}		
	}
*/

		x++;
		if(x>=15)
		{
			x=0;
			y=y+599;
			if(y>=3600)
			{
				y=0;
			}
		}

}



void CCU40_1_IRQHandler(void)
{
	if((RD_REG(CCU40_CC41->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
		if((Motor_flag==Motor_Start)||(Motor_flag==Motor_Run))
		{
//			  angle_value_in[angle_count]=Angle_feedback;
//        if(angle_count==11)
//        {
//					angle_count=0;
//					speed_get_flag=1;
//				}					
//				if(speed_get_flag==0)
//				{
//          if(angle_count>0)
//          {
//						 if(angle_value_in[angle_count]>=angle_value_in[angle_count-1])
//						 {
//								angle_gap[angle_count-1]=angle_value_in[angle_count]-angle_value_in[angle_count-1];
//						 }
//						 else
//						 {
//								angle_gap[angle_count-1]=angle_value_in[angle_count]-angle_value_in[angle_count-1]+3600;
//						 }
//						 Angle_value+=angle_gap[angle_count-1];
//					}						
//				}
//				if(speed_get_flag==1)
//				{
//					Speed=Angle_value*5/3;
//					Angle_value=0;
//				}
//				angle_count++;
				
				
        angle_value_in[angle_count]=Angle_feedback;
			  angle_count++;
			  if(angle_count==11)
				{
					  for(uint8_t i=0;i<(angle_count-1);i++)
					  {
								if(angle_value_in[i+1]>=angle_value_in[i])
								{
										angle_gap[i]=angle_value_in[i+1]-angle_value_in[i];
									  //angle_value_in[i]=0;
									  //angle_value_in[i+1]=0;
								}
								else
								{
										angle_gap[i]=angle_value_in[i+1]-angle_value_in[i]+3600;
									  //angle_value_in[i]=0;
									  //angle_value_in[i+1]=0;
								}
								Angle_value=Angle_value+angle_gap[i];
								//angle_gap[i]=0;
						}
						Speed=Angle_value*5/3;
						angle_count=0;
				}
					  Angle_value=0;
		}
		else
		{
			Speed=0;
		}
		SET_BIT(CCU40_CC41->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}


void CCU40_2_IRQHandler(void)
{
	if((RD_REG(CCU40_CC42->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
		//Angle_Adjust();
		Theta_Calculate();
		SET_BIT(CCU40_CC42->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}


void CCU40_3_IRQHandler(void)
{
	if((RD_REG(CCU40_CC43->INTS, CCU4_CC4_INTS_PMUS_Msk, CCU4_CC4_INTS_PMUS_Pos))==1)
	{
//		switch(Motor_flag)
//		{
//			case Motor_Stop:			
//			case Motor_Lock:
//			{
//				theta_start=0;
//				break;
//			}			
//			case Motor_Start:
//			{
//				theta_start=theta_start+3600-Openloop_step;
//				if(theta_start>=3600)
//				{
//					theta_start=theta_start-3600;
//				}
//				break;
//			}
//			case Motor_Run:
//			{
//				theta_start=theta_start+3600-Openloop_step;
//				if(theta_start>=3600)
//				{
//					theta_start=theta_start-3600;
//				}
//				break;
//			}
//			default :
//				break;
//		}		
		SET_BIT(CCU40_CC43->SWR, CCU4_CC4_SWR_RPM_Pos); 
	}
}


void CCU80_0_IRQHandler(void)
{
	if((RD_REG(CCU80_CC80->INTS, CCU8_CC8_INTS_OMDS_Msk, CCU8_CC8_INTS_OMDS_Pos))==1)
	{
		switch(Motor_flag)
		{
			case Motor_Stop:
			{
				CCU80_CC80->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC81->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80_CC82->CR1S	=	SVPWM16.PWM_Period+1;
				CCU80->GCSS |=0x00000111;	
				break;
			}
			case Motor_Lock:
			{
				Park_inverse(0);
				SVPWM16_7(Ua,Ub);
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;			
				CCU80->GCSS |=0x00000111;
				break;
			}
			case Motor_Start:
			{
				Park_inverse(Angle_rotor);
				SVPWM16_7(Ua,Ub);
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
				CCU80->GCSS |=0x00000111;
				break;
			}
			case Motor_Run:
			{
				Park_inverse(Angle_rotor);
				SVPWM16_7(Ua,Ub);
				CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
				CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
				CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
				CCU80->GCSS |=0x00000111;
				break;
			}
			default :
				break;

		}
//			Park_inverse(y);
//			SVPWM16_7(Ua,Ub);
//			CCU80_CC80->CR1S	=	SVPWM16.PDC_U;
//			CCU80_CC81->CR1S	=	SVPWM16.PDC_V;
//			CCU80_CC82->CR1S	=	SVPWM16.PDC_W;
//			CCU80->GCSS |=0x00000111;
		SET_BIT(CCU80_CC80->SWR, CCU8_CC8_SWR_ROM_Pos);		
	}

}


void POSIF0_0_IRQHandler(void)
{
	if((RD_REG(POSIF0->PFLG, POSIF_PFLG_CHES_Msk, POSIF_PFLG_CHES_Pos))==1)
	{
		HALL_NOW_IN=((POSIF0->PDBG)&0x00e0)>>5;

		switch(Motor_flag)
		{
			case Motor_Start:
			{
				Angle_correct(HALL_NOW_IN);
				break;
			}
			case Motor_Run:
			{
				Angle_correct(HALL_NOW_IN);
				break;
			}
			default :
				break;
		}

		SET_BIT(POSIF0->RPFLG, POSIF_RPFLG_RCHE_Pos); 
	}
	
	SET_BIT(POSIF0->RPFLG, POSIF_RPFLG_RWHE_Pos); 	
	SET_BIT(POSIF0->RPFLG, POSIF_RPFLG_RHIE_Pos);
}



void VADC0_G0_0_IRQHandler(void)
{
	if((RD_REG(VADC_G0->REFLAG, VADC_G_REFLAG_REV0_Msk, VADC_G_REFLAG_REV0_Pos))==1)
	{
		cos_threshold_flag=1;
		Cos_value=(VADC_G0->RESD[0])&0x0000FFFF;
		SET_BIT(VADC_G0->REFCLR, VADC_G_REFLAG_REV0_Pos); 
	}
  if((RD_REG(VADC_G0->REFLAG, VADC_G_REFLAG_REV6_Msk, VADC_G_REFLAG_REV6_Pos))==1)
	{
		IV_value=(VADC_G0->RESD[6])&0x0000FFFF;
		SET_BIT(VADC_G0->REFCLR, VADC_G_REFLAG_REV6_Pos); 
	}
}


void VADC0_G1_0_IRQHandler(void)
{	
	if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV0_Msk, VADC_G_REFLAG_REV0_Pos))==1)
	{
		sin_threshold_flag=1;
	  Sin_value=(VADC_G1->RESD[0])&0x0000FFFF;
		SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV0_Pos); 
	}
  
	if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV1_Msk, VADC_G_REFLAG_REV1_Pos))==1)
	{
	  R_value=(VADC_G1->RESD[1])&0x0000FFFF;						
		SET_BIT(VADC_G1->REFCLR, VADC_G_REFCLR_REV1_Pos); 
	}

  if((RD_REG(VADC_G1->REFLAG, VADC_G_REFLAG_REV6_Msk, VADC_G_REFLAG_REV6_Pos))==1)
	{
		IU_value=(VADC_G1->RESD[6])&0x0000FFFF;
		SET_BIT(VADC_G1->REFCLR, VADC_G_REFLAG_REV6_Pos); 
	}
}

