#include <XMC4400.h>
#include "POSIF.h"
#include "UART.h"
#include "ADC.h"
#include "CCU4.h"
#include "CCU8.h"
#include "GPIO.h"
#include "FOC.h"
#include "Parameter.h"
#include "HALL_Function.h"
#include "DAC.h"


int main(void)
{
	Parameter_Init();
	SysTick_Config(SystemCoreClock/1000);
	GPIO_Init();
	UART_Init();
	ADC_Init();
	POSIF0_Init();
	CCU40_Init();	
	CCU80_Init();
	//DAC_Init();
	CCU80_Start();
	POSIF0_Start();
	CCU40_CC41_Start();
	//CCU40_CC42_Start();
  CCU40_CC43_Start();
	
	
	while(1)
	{
		if(R_value<=80)
		{
			Parameter_Init();
			PID_Init();
		}

		if((Motor_flag==Motor_Stop)&&(R_value>150))
		{
//			Start_Moter();
			//Uq=-4000;
			Motor_flag=Motor_Start;		
		}
		
		if((Motor_flag==Motor_Start)&&(R_value>400))	
		{
			Motor_flag=Motor_Run;
			CCU40_CC42_Start();				
		}			
		if(Motor_flag==Motor_Run)
		{
			if(R_value<3200)
			{
				//Uq=R_value*10;				
			}
			else
			{
				//Uq=32000;
			}

		}
		if(Oscilloscope_flag)
		{
			Oscilloscope_Data[0]=IU_value;
			Oscilloscope_Data[1]=IV_value;
      Oscilloscope_Data[2]=Speed;
      Oscilloscope_Data[3]=Cos_Normalization;
//			var[0]=Sin_Normalization;
//			var[1]=Cos_Normalization;
//			var[0]=100;
//			var[1]=30;
		}
		if(sin_threshold_flag==1)
		{
			if(Sin_value>sin_max)
			{
					sin_max=Sin_value;
			}
			if(Sin_value<sin_min)
			{
					sin_min=Sin_value;
			}
			sin_threshold_flag=0;
		}
		if(cos_threshold_flag==1)
		{
			if(Cos_value>cos_max)
			{
					cos_max=Cos_value;
			}
			if(Cos_value<cos_min)
			{
					cos_min=Cos_value;
			}
			cos_threshold_flag=0;
		}
	}
}
